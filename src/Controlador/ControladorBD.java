/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.AlarmaTable;
import Modelo.AsistenteTable;
import Modelo.AvisoPeriodicidadTable;
import Modelo.AvisoTable;
import Modelo.CentrosaludTable;
import Modelo.CiudadTable;
import Modelo.ContactoTable;
import Modelo.DependienteTable;
import Modelo.EstadoTable;
import Modelo.FamiliarTable;
import Modelo.GeolocalizacionTable;
import Modelo.HistMedicoTable;
import Modelo.ListModelFamiliares;
import Modelo.ListModelNotificaciones;
import Modelo.LlamadaTable;
import Modelo.MedicoTable;
import Modelo.OfrecerRcTable;
import Modelo.ProvinciaTable;
import Modelo.RecursoComTable;
import Modelo.TableModelEstado;
import Modelo.TableModelHistorial;
import Modelo.TableModelNotificaciones;
import Modelo.VariablesEstaticas;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.swing.JComboBox;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

//Comentario
public class ControladorBD {

    private final SessionFactory connection;
    private Session bd;

    public ControladorBD() {
        connection = NewHibernateUtil.getSessionFactory();
    }

    private Session startSession() {
        Session session = connection.openSession();

        session.beginTransaction();
        return session;
    }

    public void update(Object o) {
        bd = startSession();
        bd.update(o);
        bd.getTransaction().commit();
        bd.close();

    }

    public void remove(Object o) {

        bd = startSession();
        bd.delete(o);
        bd.getTransaction().commit();
        bd.close();
    }
    //Problema en HIbernate Metodo Optimizado. Esto produce problemas cuando intentamos acceder al objecto asociado
    //ya que no cargara todos los objectos, si no accedemos a ellos en el transcurso en que la sesion esta abierta.
    //Por lo que se cargar directamente en objetos
    //Solucion indicar join fetch que te carga el objeto asociado

    public Object getUser(String campoFiltro, Class clase) {
        Object o = null;
        String query = "";
        if (clase.equals(AsistenteTable.class)) {
            query = VariablesEstaticas.consultasBD.GET_ASISTENTE;
        } else if (clase.equals(DependienteTable.class)) {
            query = VariablesEstaticas.consultasBD.GET_DEPENDIENTE;
        }

        bd = startSession();
        List res = bd.createQuery(query).setString(0, campoFiltro).list();
        if (!res.isEmpty()) {
            o = res.get(0);
        }
        bd.close();
        return o;
    }

    public ArrayList<GeolocalizacionTable> devolverCoordenadas(int id, String dia, String hora) {

        ArrayList<GeolocalizacionTable> coordenadas = new ArrayList<>();
        bd = startSession();
        List<GeolocalizacionTable> res = bd.createQuery(VariablesEstaticas.consultasBD.GET_GEOLOCALIZACION).setInteger(0, id).list();
        GeolocalizacionTable prueba = null;

        if (!res.isEmpty()) {
            Iterator it = res.iterator();

            while (it.hasNext()) {

                prueba = (GeolocalizacionTable) it.next();
                String[] campos = prueba.getFechaHora().toString().split(" ");

                if (campos[0].equals(dia)) {

                    String[] horas = campos[1].split(":");
                    String[] horasBuenas = hora.split(":");
                    if (horas[0].equals(horasBuenas[0]) && horas[1].equals(horasBuenas[1])) {

                        coordenadas.add(prueba);

                    }

                }
            }
        }
        bd.close();

        return coordenadas;
    }

    public ArrayList<DependienteTable> DelvolverTododsLosDependientes() {

        ArrayList<DependienteTable> dependientes = new ArrayList<>();
        bd = startSession();
        List<DependienteTable> res = bd.createQuery(VariablesEstaticas.consultasBD.GET_ALL_DEPENDIENTES).list();

        if (!res.isEmpty()) {
            Iterator it = res.iterator();

            while (it.hasNext()) {

                dependientes.add((DependienteTable) it.next());

            }
        }

        bd.close();

        return dependientes;
    }

    public ArrayList<GeolocalizacionTable> devolverUltimaCoordenada(int id) {

        ArrayList<GeolocalizacionTable> coordenadas = new ArrayList<>();
        bd = startSession();
        List<GeolocalizacionTable> res = bd.createQuery(VariablesEstaticas.consultasBD.GET_GEOLOCALIZACION).setInteger(0, id).list();
        bd.close();

        if (!res.isEmpty()) {
            Iterator it = res.iterator();

            while (it.hasNext()) {

                coordenadas.add((GeolocalizacionTable) it.next());

            }

        }

        return coordenadas;
    }

    public ArrayList<AvisoTable> llamadasARealizar() {

        ArrayList<AvisoTable> avisos = new ArrayList<>();
        bd = startSession();
        List<AvisoTable> res = bd.createQuery(VariablesEstaticas.consultasBD.GET_LLAMADAS_A_REALIZAR).list();

        if (!res.isEmpty()) {
            Iterator it = res.iterator();

            while (it.hasNext()) {

                avisos.add((AvisoTable) it.next());

            }
        }
        bd.close();

        return avisos;

    }

    public ArrayList<AvisoPeriodicidadTable> DevolverObjetoPeriodicidad(int id) {

        ArrayList<AvisoPeriodicidadTable> avisos = new ArrayList<>();
        bd = startSession();
        List<AvisoPeriodicidadTable> res = bd.createQuery(VariablesEstaticas.consultasBD.GET_LLAMADAS_A_REALIZAR2).setInteger(0, id).list();
        GeolocalizacionTable prueba = null;

        if (!res.isEmpty()) {
            Iterator it = res.iterator();

            while (it.hasNext()) {

                avisos.add((AvisoPeriodicidadTable) it.next());

            }
        }
        bd.close();

        return avisos;

    }

    public OfrecerRcTable devolverOfrecerRc(DependienteTable dependiente, int i) {

        ArrayList<OfrecerRcTable> ofrecer = new ArrayList<>();
        bd = startSession();
        List<OfrecerRcTable> res = bd.createQuery(VariablesEstaticas.consultasBD.GET_OFRECER_RC).setInteger(0, dependiente.getId()).list();
        OfrecerRcTable prueba = null;
        OfrecerRcTable devolver = null;
        if (!res.isEmpty()) {

            Iterator it = res.iterator();

            while (it.hasNext()) {

                prueba = (OfrecerRcTable) it.next();

                if (prueba.getRecursoComTable().getId() == i) {

                    devolver = prueba;

                }
            }
        }
        bd.close();
        return devolver;
    }

    public ArrayList<ProvinciaTable> devolverProvincias() {

        ArrayList<ProvinciaTable> ofrecer = new ArrayList<>();
        bd = startSession();
        List<OfrecerRcTable> res = bd.createQuery(VariablesEstaticas.consultasBD.GET_PROVINCIAS).list();

        if (!res.isEmpty()) {

            Iterator it = res.iterator();

            while (it.hasNext()) {

                ofrecer.add((ProvinciaTable) it.next());

            }
        }

        bd.close();
        return ofrecer;
    }

    public ArrayList<LlamadaTable> DevolverTodasLlamadasHistorial() {

        bd = startSession();

        List<LlamadaTable> res = bd.createQuery(VariablesEstaticas.consultasBD.GET_TODAS_LLAMADAS_HISTORIAL).list();

        ArrayList<LlamadaTable> ofrecer = new ArrayList<>(res);

        bd.close();
        return ofrecer;
    }

    public ArrayList<LlamadaTable> DevolverTodasLlamadasHistorialDependiente(int id) {

        bd = startSession();
        List<LlamadaTable> res = bd.createQuery(VariablesEstaticas.consultasBD.GET_LLAMADAS_DEPENDIENTE).setInteger(0, id).list();
        bd.close();
        ArrayList<LlamadaTable> ofrecer = new ArrayList<>(res);
        return ofrecer;
    }

    public ArrayList<AlarmaTable> DevolverTodasAlarmasHistorial() {

        bd = startSession();

        List<AlarmaTable> res = bd.createQuery(VariablesEstaticas.consultasBD.GET_TODAS_ALARMAS).list();

        ArrayList<AlarmaTable> ofrecer = new ArrayList<>(res);

        bd.close();
        return ofrecer;
    }

    public ArrayList<CiudadTable> devolverCiudades(int id) {

        ArrayList<CiudadTable> ofrecer = new ArrayList<>();
        bd = startSession();

        if (id == -1) {

            List<CiudadTable> res = bd.createQuery(VariablesEstaticas.consultasBD.GET_ALL_CIUDADES).list();

            if (!res.isEmpty()) {

                Iterator it = res.iterator();

                while (it.hasNext()) {

                    ofrecer.add((CiudadTable) it.next());

                }
            }
        } else {

            List<CiudadTable> res = bd.createQuery(VariablesEstaticas.consultasBD.GET_CIUDADES_PROVINCIA).setInteger(0, id).list();
            CiudadTable prueba = null;

            if (!res.isEmpty()) {

                Iterator it = res.iterator();

                while (it.hasNext()) {
                    prueba = (CiudadTable) it.next();

                    if (prueba.getProvinciaTable().getId() == id) {

                        ofrecer.add(prueba);
                    }
                }
            }
        }

        bd.close();
        return ofrecer;
    }

    public int devolverIdRecurso(String recurso) {

        bd = startSession();

        List<RecursoComTable> res = bd.createQuery(VariablesEstaticas.consultasBD.GET_ID_RECURSO).setString(0, recurso).list();
        int prueba = 0;
        if (!res.isEmpty()) {

            prueba = res.get(0).getId();

        }

        bd.close();
        return prueba;
    }

    public void insert(Object o) {
        bd = startSession();
        bd.save(o);
        bd.getTransaction().commit();
        bd.close();
    }

    public void consultar(JComboBox comboBox, Class clase) {
        String query = "";
        if (clase == MedicoTable.class) {
            query = VariablesEstaticas.consultasBD.GET_MEDICO;
        } else if (clase == CentrosaludTable.class) {
            query = VariablesEstaticas.consultasBD.GET_CENTROSALUD;
        }
        bd = startSession();
        List res = bd.createQuery(query).list();
        comboBox.removeAllItems();
        if (!res.isEmpty()) {
            Iterator it = res.iterator();
            while (it.hasNext()) {
                comboBox.addItem(it.next());
            }
        }
        bd.close();
    }

    public void consultarViviendas(JComboBox comboBox, int id) {
        bd = startSession();
        List res = bd.createQuery(VariablesEstaticas.consultasBD.GET_VIVIENDAS).setInteger(0, id).list();
        bd.close();
        comboBox.removeAllItems();
        if (!res.isEmpty()) {
            Iterator it = res.iterator();
            while (it.hasNext()) {
                comboBox.addItem(it.next());
            }
        }
//      
    }

    private void llenarCombo(JComboBox comboBox, List res) {
        comboBox.removeAllItems();
        if (!res.isEmpty()) {
            Iterator it = res.iterator();
            while (it.hasNext()) {
                comboBox.addItem(it.next());
            }
        }
    }

    public void llenarTablaHistorial(TableModelHistorial hist, int id) {
        bd = startSession();
        List res = bd.createQuery(VariablesEstaticas.consultasBD.GET_HISTORIAL_MEDICO).setInteger(0, id).list();
        Iterator it = res.iterator();
        while (it.hasNext()) {
            hist.addRow((HistMedicoTable) it.next());
        }
        bd.close();

    }

    public void llenarTablaEstado(TableModelEstado hist, int id) {
        bd = startSession();
        List res = bd.createQuery(VariablesEstaticas.consultasBD.GET_ESTADOS).setInteger(0, id).list();
        Iterator it = res.iterator();
        while (it.hasNext()) {
            hist.addRow((EstadoTable) it.next());
        }
        bd.close();

    }

    public void getContactos(JComboBox comboBox, ListModelFamiliares list, int id) {
        bd = startSession();
        List l = bd.createQuery(VariablesEstaticas.consultasBD.GET_CONTACTOS).setInteger(0, id).list();
        bd.close();
        if (!l.isEmpty()) {
            Iterator it = l.iterator();
            while (it.hasNext()) {
                list.addElement((ContactoTable) it.next());
            }
        }
        if (!l.isEmpty()) {
            Iterator it = l.iterator();
            while (it.hasNext()) {
                comboBox.addItem(it.next());
            }
        }

//
    }

    public ArrayList<ContactoTable> getContactosEmergencia(int id) {
        bd = startSession();
        List res = bd.createQuery(VariablesEstaticas.consultasBD.GET_CONTACTOS_EMERGENCIA).setInteger(0, id).list();
        ArrayList<ContactoTable> contactos = new ArrayList<>(res);

        bd.close();
        return contactos;
    }

    public void llenarAvisos(ListModelNotificaciones list, int id) {
        list.removeAll();
        bd = startSession();
        List res = bd.createQuery(VariablesEstaticas.consultasBD.GET_AVISO).setInteger(0, id).list();
        bd.close();

        Iterator it = res.iterator();
        while (it.hasNext()) {
            list.addElement((AvisoTable) it.next());
        }

    }

    public void llenarAvisosPeriodicidad(TableModelNotificaciones list, int id) {
        bd = startSession();
        List res = bd.createQuery(VariablesEstaticas.consultasBD.GET_AVISO_PERIODICIDAD).setInteger(0, id).list();
        Iterator it = res.iterator();
        while (it.hasNext()) {
            list.addRow((AvisoPeriodicidadTable) it.next());
        }
        bd.close();
    }
}
