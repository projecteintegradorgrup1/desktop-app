/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.ControladorBD;
import Modelo.AsistenteTable;
import Modelo.ContactoTable;
import Modelo.LlamadaTable;
import Modelo.ModeloDeFamiliares;
import Modelo.ModeloDeLlamadas;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JOptionPane;

public class EmergenciaFamiliar extends javax.swing.JFrame {

    private ArrayList<ContactoTable> contactos;
    private ArrayList<ContactoTable> contactos2;
    private ModeloDeFamiliares modelo;
    private ControladorBD bd;
    private AsistenteTable asistente;

    public EmergenciaFamiliar(int id, AsistenteTable asistente) {
        initComponents();
        bd = new ControladorBD();
        contactos = new ArrayList<>();
        contactos2 = new ArrayList<>();
        contactos = bd.getContactosEmergencia(id);
        modelo = new ModeloDeFamiliares();
        this.asistente = asistente;

        insertTablaFamiliares();

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTableFamimlia = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jTableFamimlia.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTableFamimlia);

        jButton1.setText("Aceptar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("CANCELAR");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 482, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(50, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(86, 86, 86)
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton2)
                .addGap(139, 139, 139))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 345, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addContainerGap(43, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed

        dispose();

    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        if (jTableFamimlia.getSelectedRow() != -1) {

            ContactoTable Contacto = contactos.get(jTableFamimlia.getSelectedRow());
            JOptionPane.showMessageDialog(null, "Llamando a: " + Contacto.getFamiliarTable().getPersonaTable().getNombre() + " telf: " + Contacto.getFamiliarTable().getPersonaTable().getTelefono());

            java.util.Date utilDate = new java.util.Date();
            long lnMilisegundos = utilDate.getTime();
            java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(lnMilisegundos);
            
            LlamadaTable llamada = new LlamadaTable(asistente, null, (Date) sqlTimestamp, "Llamada de Emergencia a un Familiar");
            bd.insert(llamada);
            dispose();

        }

    }//GEN-LAST:event_jButton1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTableFamimlia;
    // End of variables declaration//GEN-END:variables

    private void insertTablaFamiliares() {

        for (int i = 0; i < contactos.size(); i++) {

            Object[] fila = new Object[3];
            fila[0] = contactos.get(i).getPrioridad();
            fila[1] = contactos.get(i).getFamiliarTable().getPersonaTable().getNombreCompleto();
            fila[2] = contactos.get(i).getFamiliarTable().getDisponibilidad();
            modelo.addRow(fila);

        }
        jTableFamimlia.setModel(modelo);
    }
}
