/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.ControladorBD;
import Modelo.GeolocalizacionTable;
import Modelo.Mapa_Geolocalizacion;
import com.teamdev.jxbrowser.chromium.Browser;
import java.awt.BorderLayout;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

public class SeguimientoGeolocalizacion extends javax.swing.JFrame {

    
    private ControladorBD bd;
    private ArrayList<GeolocalizacionTable> coordenadas;
    private Double latitud;
    private Double longitud;

    public SeguimientoGeolocalizacion(Double latitud, Double longitud) throws ParseException {

        //coordenadas = new ArrayList<>();
        //this.mapa = mapa;
        bd = new ControladorBD();

        initComponents();
        this.latitud = latitud;
        this.longitud = longitud;

        
        ponerMapa(latitud, longitud);
        //labelMapa.setIcon(mapa.getImagen());

    }

    
    public void ponerMapa(Double latitud, Double longitud) {

        final Browser browser = new Browser();
        com.teamdev.jxbrowser.chromium.swing.BrowserView view = new com.teamdev.jxbrowser.chromium.swing.BrowserView(browser);
        panelMapa.setLayout(new BorderLayout());
        panelMapa.add(view, BorderLayout.CENTER);
        panelMapa.setVisible(true);
        //browser.loadURL("https://www.google.com/maps/place/23%C2%B008'06.9%22N+82%C2%B021'34.9%22W/@23.135249,-82.3618737,17z/data=!3m1!4b1!4m5!3m4!1s0x0:0x0!8m2!3d23.135249!4d-82.359685");
        browser.loadURL("https://maps.google.com/?q=" + latitud + "," + longitud);

    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelMapa = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout panelMapaLayout = new javax.swing.GroupLayout(panelMapa);
        panelMapa.setLayout(panelMapaLayout);
        panelMapaLayout.setHorizontalGroup(
            panelMapaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1165, Short.MAX_VALUE)
        );
        panelMapaLayout.setVerticalGroup(
            panelMapaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 637, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelMapa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelMapa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel panelMapa;
    // End of variables declaration//GEN-END:variables
}
