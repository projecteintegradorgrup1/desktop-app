
package Vista;

import Controlador.ControladorBD;
import Modelo.AlarmaTable;
import Modelo.LlamadaTable;
import Modelo.ModeloHistorialAlarma;
import Modelo.ModeloHistorialLlamadas;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Historial extends javax.swing.JPanel {

    private ModeloHistorialLlamadas modelo;
    private ModeloHistorialAlarma modelo2;
    private ArrayList<LlamadaTable> historicoLlamadas;
    private ArrayList<AlarmaTable> historicoAlarmas;
    private ControladorBD bd;
    
    public Historial() {
        initComponents();
        
        modelo = new ModeloHistorialLlamadas();
        modelo2 = new ModeloHistorialAlarma();
        jtHistorialLlamadas.setModel(modelo);
        jtHistorialAlarmaas.setModel(modelo2);
        
        jtHistorialLlamadas.getColumnModel().getColumn(0).setPreferredWidth(30);
        jtHistorialLlamadas.getColumnModel().getColumn(1).setPreferredWidth(90);
        jtHistorialLlamadas.getColumnModel().getColumn(2).setPreferredWidth(170);
        jtHistorialLlamadas.getColumnModel().getColumn(3).setPreferredWidth(702);
        
        jtHistorialAlarmaas.getColumnModel().getColumn(0).setPreferredWidth(30);
        jtHistorialAlarmaas.getColumnModel().getColumn(1).setPreferredWidth(160);
        jtHistorialAlarmaas.getColumnModel().getColumn(2).setPreferredWidth(170);
        jtHistorialAlarmaas.getColumnModel().getColumn(3).setPreferredWidth(170);
        jtHistorialAlarmaas.getColumnModel().getColumn(4).setPreferredWidth(464);
        
        bd = new ControladorBD();
        historicoLlamadas = new ArrayList<>();
        historicoAlarmas = new ArrayList<>();
        
        insertarEnTablaHistorialLlamadas();
        insertarEnTablaHistorialAlarmas();
        
        
        
    }


    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtHistorialAlarmaas = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        jtHistorialLlamadas = new javax.swing.JTable();

        setPreferredSize(new java.awt.Dimension(1415, 695));

        jLabel1.setText("Historial de llamadas: ");

        jLabel2.setText("Historial de alarmas:");

        jtHistorialAlarmaas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jtHistorialAlarmaas.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jScrollPane1.setViewportView(jtHistorialAlarmaas);

        jtHistorialLlamadas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jtHistorialLlamadas.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jScrollPane2.setViewportView(jtHistorialLlamadas);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(55, 55, 55)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2)
                    .addComponent(jLabel1)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1000, Short.MAX_VALUE)
                    .addComponent(jScrollPane2))
                .addContainerGap(360, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 295, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 278, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jtHistorialAlarmaas;
    private javax.swing.JTable jtHistorialLlamadas;
    // End of variables declaration//GEN-END:variables

    private void insertarEnTablaHistorialLlamadas() {
        historicoLlamadas = bd.DevolverTodasLlamadasHistorial();
        
        for (int i = 0; i < historicoLlamadas.size(); i++) {
            
            SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            //Date dia = historicoLlamadas.get(i).getFechaHora();
            
            Object[] fila = new Object[4];
            fila[0] = historicoLlamadas.get(i).getId();
            fila[1] = historicoLlamadas.get(i).getFechaHora();;
            fila[2] = historicoLlamadas.get(i).getAsistenteTable().getPersonaTable().getNombreCompleto();
            //fila[3] = historicoLlamadas.get(i).getAvisoTable().getId();
            fila[3] = historicoLlamadas.get(i).getRazonLlamada();

            modelo.addRow(fila);
            
        }
         
    }

    private void insertarEnTablaHistorialAlarmas() {
        
        historicoAlarmas = bd.DevolverTodasAlarmasHistorial();
        
        for (int i = 0; i < historicoAlarmas.size(); i++) {
            
            Object[] fila = new Object[5];
            fila[0] = historicoAlarmas.get(i).getId();
            fila[1] = historicoAlarmas.get(i).getDependienteTable().getPersonaTable().getNombreCompleto();
            fila[2] = historicoAlarmas.get(i).getFechaHora();
            fila[3] = historicoAlarmas.get(i).getAsistenteTable().getPersonaTable().getNombreCompleto();
            fila[4] = historicoAlarmas.get(i).getMotivoAlarma();

            modelo2.addRow(fila);
            
        }
        
    }
}
