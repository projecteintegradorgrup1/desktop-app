/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.ControladorBD;
import Modelo.AvisoPeriodicidadTable;
import Modelo.AvisoTable;
import Modelo.CentrosaludTable;
import Modelo.CiudadTable;
import Modelo.ContactoTable;
import Modelo.DependienteTable;
import Modelo.DireccionTable;
import Modelo.EstadoTable;
import Modelo.FamiliarTable;
import Modelo.FilterComboBox;
import Modelo.GeolocalizacionTable;
import Modelo.HistMedicoTable;
import Modelo.ListModelFamiliares;
import Modelo.ListModelNotificaciones;
import Modelo.LlamadaTable;
import Modelo.Mapa_Geolocalizacion;
import Modelo.MedicoTable;
import Modelo.ModeloHistorialLlamadas;
import Modelo.PersonaTable;
import Modelo.ViviendaTable;
import Modelo.ProvinciaTable;
import Modelo.TableModelEstado;
import Modelo.TableModelHistorial;
import Modelo.TableModelNotificaciones;
import Modelo.VariablesEstaticas;
import com.teamdev.jxbrowser.chromium.Browser;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author vesprada
 */
public final class Expediente extends javax.swing.JPanel {

    /**
     * Creates new form Expediente
     */
    private ControladorBD bd;
    public DependienteTable dependiente;

    private boolean nuevoRegistro;
    public Double latitud = 38.693656718369255;
    public Double longitud = -0.4846062868726335;
    public int ID;
    private Mapa_Geolocalizacion mapa;
    public ArrayList<GeolocalizacionTable> geo;
    private FilterComboBox jcbProvincia;
    private FilterComboBox jcbCiudad;
    private FilterComboBox jcbProvinciaFamiliar;
    private FilterComboBox jcbCiudadFamiliar;
    private TableModelHistorial tableHistorial;
    private ModeloHistorialLlamadas modeloLlamdas;
    private TableModelEstado tableEstado;
    private ListModelNotificaciones listNotificaciones;
    private TableModelNotificaciones tableNotificaciones;
    private AvisoTable avisoTmp;
    private HistMedicoTable historialTmp;
    private EstadoTable estadoTmp;
    private AvisoPeriodicidadTable recordatorioTmp;
    private ListModelFamiliares listFamiliares;
    private VistaPrincipal vista;
    private PasswordDependiente vistaPassword;

    public Expediente(DependienteTable dependiente, VistaPrincipal vista) {
        bd = new ControladorBD();
        initComponents();
        disableButtonEdit();
        initCustomComponents();

        initJTables();
        this.dependiente = dependiente;
        this.vista = vista;
        bd.consultar(jcbMedico, MedicoTable.class);

        jcbMedico.setSelectedIndex(-1);
        modeloLlamdas = new ModeloHistorialLlamadas();

        bd.consultar(jcbCentroSalud, CentrosaludTable.class);
        jcbCentroSalud.setSelectedIndex(-1);
        if (this.dependiente == null) {
            nuevoRegistro = true;
            nuevoExpedinete();

        } else {
            cambiarEstadoViviendas(false);
            bd.consultarViviendas(jcbViviendaActual, dependiente.getId());
            bd.getContactos(jcbContactos, listFamiliares, dependiente.getId());
            bd.consultarViviendas(jcbViviendas, dependiente.getId());

            cargarDatosDependiente();
            cargarDatosVivienda();
            cargarDatosContactos();

            bd.llenarTablaHistorial(tableHistorial, dependiente.getId());
            bd.llenarTablaEstado(tableEstado, dependiente.getId());
            bd.llenarAvisos(listNotificaciones, dependiente.getId());

        }

        cambiarEstadoRecordatorio(false);
        cambiarEstadoHistorial(false);
        cambiarEstadoFamiliar(false);
        cambiarEstadoNotificacion(false);

        if (this.dependiente != null) {

            initMaps();

            jTableLlamadas.setModel(modeloLlamdas);
            jTableLlamadas.getColumnModel().getColumn(0).setPreferredWidth(30);
            jTableLlamadas.getColumnModel().getColumn(1).setPreferredWidth(90);
            jTableLlamadas.getColumnModel().getColumn(2).setPreferredWidth(200);
            jTableLlamadas.getColumnModel().getColumn(3).setPreferredWidth(600);
            
            initTablaLlamadasAll();

        }
        if (this.dependiente == null) {
            tabbedPaneGeneral.setEnabled(false);
        }

    }

    private void initJTables() {
        tableHistorial = new TableModelHistorial();
        jtableHistorial.setModel(tableHistorial);
        jtableHistorial.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jtableHistorial.getColumnModel().getColumn(0).setPreferredWidth(90);
        jtableHistorial.getColumnModel().getColumn(1).setPreferredWidth(600);

        tableEstado = new TableModelEstado();
        jTableEstado.setModel(tableEstado);
        jTableEstado.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jTableEstado.getColumnModel().getColumn(0).setPreferredWidth(105);
        jTableEstado.getColumnModel().getColumn(1).setPreferredWidth(105);
        jTableEstado.getColumnModel().getColumn(2).setPreferredWidth(600);
        listNotificaciones = new ListModelNotificaciones();
        jlistNotificaciones.setModel(listNotificaciones);
        jlistNotificaciones.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        tableNotificaciones = new TableModelNotificaciones();
        jTableNotifiaciones.setModel(tableNotificaciones);
        jTableNotifiaciones.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jTableNotifiaciones.getColumnModel().getColumn(0).setPreferredWidth(15);
        jTableNotifiaciones.getColumnModel().getColumn(1).setPreferredWidth(250);

        listFamiliares = new ListModelFamiliares();
        jlistFamiliares.setModel(listFamiliares);
        jlistFamiliares.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

    public void initMaps() {

        ID = dependiente.getId();
        geo = bd.devolverUltimaCoordenada(ID);
        if (!geo.isEmpty()) {
            labelMapa.setText("");
            mapa = new Mapa_Geolocalizacion((geo.get(geo.size() - 1)).getLatitud(), (geo.get(geo.size() - 1)).getLongitud());

            labelMapa.setIcon(mapa.getImagen());

            TimerTask timerTask = new TimerTask() {
                public void run() {
                    try {
                        geo = bd.devolverUltimaCoordenada(ID);
                        mapa.asignarCoordenadaEspecifica((geo.get(geo.size() - 1)).getLatitud(), (geo.get(geo.size() - 1)).getLongitud());
                        mapa.RecargarImagen();
                        labelMapa.setIcon(mapa.getImagen());
                    } catch (IOException ex) {
                        Logger.getLogger(Expediente.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            };

            Timer timer = new Timer();
            timer.scheduleAtFixedRate(timerTask, 0, 60000);
        } else {

            labelMapa.setIcon(null);
            labelMapa.setText("Este dependiente no tiene una Localizacion asignada");
            labelMapa.setVerticalAlignment(SwingConstants.CENTER);
            labelMapa.setHorizontalAlignment(SwingConstants.CENTER);

        }

    }

    private void initCustomComponents() {

        //Creamos un Array
        List<Object> array = new ArrayList<>();
        //Devolvemos las provincias
        ArrayList<ProvinciaTable> provinciasDeLaTabla = bd.devolverProvincias();

        //Añadimos un primer elemento en blanco
        array.add(new ProvinciaTable(99999999, ""));
        //Añadimos las provincias al List
        for (int i = 0; i < provinciasDeLaTabla.size(); i++) {

            array.add(provinciasDeLaTabla.get(i));

        }

        jcbProvincia = new FilterComboBox(array);
        jcbProvinciaFamiliar = new FilterComboBox(array);

        //Añadimos el Listener al objeto
        jcbProvincia.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {

                //Eliminamos todo del panel
                panelPostal.removeAll();

                //jcbCiudad.setEnabled(true);
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    //Cargamos el array de ciudades segun la provincia
                    List<Object> array2 = new ArrayList<>();
                    try {
                        ProvinciaTable item = (ProvinciaTable) jcbProvincia.getSelectedItem();
                        ArrayList<CiudadTable> ciudades = bd.devolverCiudades(item.getId());

                        for (int i = 0; i < ciudades.size(); i++) {

                            array2.add(ciudades.get(i));

                        }

                        //Creamos el objeto FilterComboBox pasandole el array de Ciudades
                        jcbCiudad = new FilterComboBox(array2);

                        //Le asignamos el ancho y largo
                        jcbCiudad.setBounds(0, 0, 250, 30);
                        //Lo añadimos al panel
                        panelPostal.add(jcbCiudad);
                    } catch (ClassCastException ex) {

                    }

                }

            }
        });

        jcbProvinciaFamiliar.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {

                //Eliminamos todo del panel
                panelPostalFamilia.removeAll();

                //jcbCiudad2.setEnabled(true);
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    try {
                        List<Object> array3 = new ArrayList<>();
                        ProvinciaTable item = (ProvinciaTable) jcbProvinciaFamiliar.getSelectedItem();

                        ArrayList<CiudadTable> ciudades = bd.devolverCiudades(item.getId());

                        for (int i = 0; i < ciudades.size(); i++) {

                            array3.add(ciudades.get(i));

                        }

                        //Creamos el objeto FilterComboBox pasandole el array de Ciudades
                        jcbCiudadFamiliar = new FilterComboBox(array3);
                        //Le asignamos el ancho y largo
                        jcbCiudadFamiliar.setBounds(0, 0, 250, 30);
                        //Lo añadimos al panel
                        panelPostalFamilia.add(jcbCiudadFamiliar);

                    } catch (ClassCastException ex) {

                    }

                    //Cargamos el array de ciudades segun la provincia
                }

            }
        });

        //Creamos el objeto FilterComboBox pasandole el array de Ciudades
        jcbProvincia.setBounds(0, 0, 150, 30);
        jcbProvinciaFamiliar.setBounds(0, 0, 150, 30);
        //Añadimos el objeto de JComboProvincias al panel
        panelProv.add(jcbProvincia);
        panelProvFamilia.add(jcbProvinciaFamiliar);

        //Funcionalidad JCombos
        jcbViviendas.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cargarDatosVivienda();
            }
        });
        jcbContactos.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cargarDatosContactos();
            }
        });

        jlistNotificaciones.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent lse) {
                cargarDatosAviso();
            }
        });

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane4 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        tfApellido1 = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        tfDDni = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        tfTelefono = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jcbViviendaActual = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        tfFechaAlta = new datechooser.beans.DateChooserCombo();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        tfNss = new javax.swing.JTextField();
        jcbGenero = new javax.swing.JComboBox<>();
        jLabel11 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        tfApellido2 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        tfNombre = new javax.swing.JTextField();
        tfNacimiento = new datechooser.beans.DateChooserCombo();
        jLabel6 = new javax.swing.JLabel();
        tfEmail = new javax.swing.JTextField();
        jLabel50 = new javax.swing.JLabel();
        jcbMedico = new javax.swing.JComboBox<>();
        btnAceptarGeneral = new javax.swing.JButton();
        btnCancelarGeneral = new javax.swing.JButton();
        btnEditarGeneral = new javax.swing.JButton();
        jLabel12 = new javax.swing.JLabel();
        jcbCentroSalud = new javax.swing.JComboBox<>();
        btnEstablecerPasswd = new javax.swing.JButton();
        labelReaload = new javax.swing.JLabel();
        tabbedPaneGeneral = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        jcbViviendas = new javax.swing.JComboBox<>();
        btnAgregarVivienda = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        jcbTipoVia = new javax.swing.JComboBox<>();
        jLabel16 = new javax.swing.JLabel();
        tfDireccion = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        tfNumero = new javax.swing.JTextField();
        tfPiso = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        tfLetra = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        tfEscalera = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jcbTipoVivienda = new javax.swing.JComboBox<>();
        jLabel25 = new javax.swing.JLabel();
        tfModoAcceso = new javax.swing.JTextField();
        btnAceptarVivienda = new javax.swing.JButton();
        btnCancelaVivienda = new javax.swing.JButton();
        panelProv = new javax.swing.JPanel();
        panelPostal = new javax.swing.JPanel();
        jLabel26 = new javax.swing.JLabel();
        btnEliminarVivienda = new javax.swing.JButton();
        btnEditarViviendas = new javax.swing.JButton();
        panelH = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        jtableHistorial = new javax.swing.JTable();
        jLabel51 = new javax.swing.JLabel();
        btnNuevoRegistroMedico = new javax.swing.JButton();
        btnEliminarRegistroMed = new javax.swing.JButton();
        btnAceptarHistorial = new javax.swing.JButton();
        btnCancelarHisotial = new javax.swing.JButton();
        btnEditarHistorial = new javax.swing.JButton();
        panelHistorialEdit = new javax.swing.JPanel();
        jLabel41 = new javax.swing.JLabel();
        tfTipoHistorial = new javax.swing.JTextField();
        jLabel48 = new javax.swing.JLabel();
        tfDescripciónHistorial = new javax.swing.JTextField();
        panelEstado = new javax.swing.JPanel();
        jLHoraInicio = new javax.swing.JLabel();
        jlFechaInicio = new javax.swing.JLabel();
        tfFechaInicio = new datechooser.beans.DateChooserCombo();
        tfHoraInicio = new javax.swing.JTextField();
        tfMinutoInicio = new javax.swing.JTextField();
        jLMinutoInicio = new javax.swing.JLabel();
        jScrollPane7 = new javax.swing.JScrollPane();
        jTableEstado = new javax.swing.JTable();
        btnAgregarEstado = new javax.swing.JButton();
        btnEliminarEstado = new javax.swing.JButton();
        btnAceptarEstado = new javax.swing.JButton();
        btnCancelarEstado = new javax.swing.JButton();
        jLFechaFin = new javax.swing.JLabel();
        tfFechaFin = new datechooser.beans.DateChooserCombo();
        jLHoraFin = new javax.swing.JLabel();
        tfHoraFin = new javax.swing.JTextField();
        tfMinutoFin = new javax.swing.JTextField();
        jLMinutoFin = new javax.swing.JLabel();
        jLdescripcionEstado = new javax.swing.JLabel();
        btnEditarEstado = new javax.swing.JButton();
        panelDescripcion = new javax.swing.JPanel();
        jScrollPane8 = new javax.swing.JScrollPane();
        tfDescripcionEstado = new javax.swing.JTextArea();
        jLabel53 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jcbContactos = new javax.swing.JComboBox<>();
        jPanel8 = new javax.swing.JPanel();
        jLabel29 = new javax.swing.JLabel();
        tfDniFamiliar = new javax.swing.JTextField();
        jLabel30 = new javax.swing.JLabel();
        tfApellido1Familiar = new javax.swing.JTextField();
        jLabel31 = new javax.swing.JLabel();
        tfApellido2Familiar = new javax.swing.JTextField();
        jLabel32 = new javax.swing.JLabel();
        tfNombreFamiliar = new javax.swing.JTextField();
        jLabel33 = new javax.swing.JLabel();
        tfTelefonoFamilia = new javax.swing.JTextField();
        cbTieneLlaves = new javax.swing.JCheckBox();
        jLabel34 = new javax.swing.JLabel();
        tfEmailFamiliar = new javax.swing.JTextField();
        jLabel35 = new javax.swing.JLabel();
        tfDisponibilidadfamiliar = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        tfObreservaciones = new javax.swing.JTextField();
        jLabel38 = new javax.swing.JLabel();
        jcbTipoViaFamiliar = new javax.swing.JComboBox<>();
        tfDireccionFamiliar = new javax.swing.JTextField();
        jLabel42 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        tfNumeroFamiliar = new javax.swing.JTextField();
        tfPisoFamiliar = new javax.swing.JTextField();
        jLabel44 = new javax.swing.JLabel();
        tfLetraFamiliar = new javax.swing.JTextField();
        jLabel45 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        tfEscaleraFamiliar = new javax.swing.JTextField();
        btnAceptarFamiliar = new javax.swing.JButton();
        btnCancelarFamiliar = new javax.swing.JButton();
        panelProvFamilia = new javax.swing.JPanel();
        jLabel39 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        panelPostalFamilia = new javax.swing.JPanel();
        jLabel28 = new javax.swing.JLabel();
        tfRelación = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        jLabel52 = new javax.swing.JLabel();
        btnAgregarFamiliar = new javax.swing.JButton();
        btnEliminarFamiliar = new javax.swing.JButton();
        btnEditarFamiliar = new javax.swing.JButton();
        jScrollPane9 = new javax.swing.JScrollPane();
        jlistFamiliares = new javax.swing.JList<>();
        btnArriba = new javax.swing.JButton();
        btnAbajo = new javax.swing.JButton();
        jLabel13 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel37 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jlistNotificaciones = new javax.swing.JList<>();
        jLabel47 = new javax.swing.JLabel();
        tfNombreNotificacion = new javax.swing.JTextField();
        jLabel49 = new javax.swing.JLabel();
        jcbTipoNotificacion = new javax.swing.JComboBox<>();
        labelFechaInicio = new javax.swing.JLabel();
        labelFechaFin = new javax.swing.JLabel();
        tfFechaInicioNot = new datechooser.beans.DateChooserCombo();
        tfFechaFinNot = new datechooser.beans.DateChooserCombo();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTableNotifiaciones = new javax.swing.JTable();
        bntNuevaNotificacion = new javax.swing.JButton();
        btnBorrarNotificacion = new javax.swing.JButton();
        btnAgregarRocordatorio = new javax.swing.JButton();
        btnEliminarRecordatorio = new javax.swing.JButton();
        btnAceparNotificacion = new javax.swing.JButton();
        btnCancelarNotificacion = new javax.swing.JButton();
        btnEditarNotificacion = new javax.swing.JButton();
        btnEditarRecordatorio = new javax.swing.JButton();
        panelRecordatorio = new javax.swing.JPanel();
        tfHoraInicioNot = new javax.swing.JTextField();
        tfMinutoInicioNot = new javax.swing.JTextField();
        jLMinutoInicio1 = new javax.swing.JLabel();
        jLHoraInicio1 = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tfDescripcionNotificacion = new javax.swing.JTextArea();
        btnAceptarRecordatorio = new javax.swing.JButton();
        btnCancelarRecordatorio = new javax.swing.JButton();
        cbFechaFin = new javax.swing.JCheckBox();
        jPanel6 = new javax.swing.JPanel();
        btnSeguimiento = new javax.swing.JButton();
        jPanel10 = new javax.swing.JPanel();
        btnActualizarMapa = new javax.swing.JButton();
        labelMapa = new javax.swing.JLabel();
        btnZOOMMas = new javax.swing.JButton();
        btnZOOMMenos = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableLlamadas = new javax.swing.JTable();

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane4.setViewportView(jTable2);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos del paciente"));

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("1º Apellido:");

        tfApellido1.setEditable(false);
        tfApellido1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfApellido1ActionPerformed(evt);
            }
        });

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("DNI:");

        tfDDni.setEditable(false);

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("Teléfono:");

        tfTelefono.setEditable(false);

        jLabel7.setText("Vivienda actual:");

        jcbViviendaActual.setEnabled(false);

        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("F. Nac:");

        tfFechaAlta.setCalendarPreferredSize(new java.awt.Dimension(350, 220));
        tfFechaAlta.setNothingAllowed(false);
        tfFechaAlta.setEnabled(false);

        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("Fecha de alta:");

        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("Género:");

        tfNss.setEditable(false);
        tfNss.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfNssActionPerformed(evt);
            }
        });

        jcbGenero.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Hombre", "Mujer" }));
        jcbGenero.setEnabled(false);

        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel11.setText("Número seguridad social:");

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("2º Apellido:");

        tfApellido2.setEditable(false);

        jLabel1.setText("Nombre:");

        tfNombre.setEditable(false);

        tfNacimiento.setCalendarPreferredSize(new java.awt.Dimension(350, 220));
        tfNacimiento.setNothingAllowed(false);
        tfNacimiento.setEnabled(false);

        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("Email:");

        tfEmail.setEditable(false);

        jLabel50.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel50.setText("Médico:");

        jcbMedico.setEnabled(false);

        btnAceptarGeneral.setText("Aceptar");
        btnAceptarGeneral.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarGeneralActionPerformed(evt);
            }
        });

        btnCancelarGeneral.setText("Cancelar");
        btnCancelarGeneral.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarGeneralActionPerformed(evt);
            }
        });

        btnEditarGeneral.setText("Editar");
        btnEditarGeneral.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarGeneralActionPerformed(evt);
            }
        });

        jLabel12.setText("Centro de salud:");

        jcbCentroSalud.setEnabled(false);

        btnEstablecerPasswd.setText("Establecer contraseña");
        btnEstablecerPasswd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEstablecerPasswdActionPerformed(evt);
            }
        });

        labelReaload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Vista/Imagens/reload.png"))); // NOI18N
        labelReaload.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        labelReaload.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                labelRealoadMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tfDDni, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jcbGenero, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(tfApellido1, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(tfTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(tfApellido2)
                    .addComponent(tfNacimiento, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tfEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 267, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tfNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(labelReaload)
                .addGap(37, 37, 37))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(tfFechaAlta, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(tfNss, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jcbCentroSalud, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jcbViviendaActual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel50)
                        .addGap(18, 18, 18)
                        .addComponent(jcbMedico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnEstablecerPasswd)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(btnAceptarGeneral)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnEditarGeneral)
                            .addComponent(btnCancelarGeneral))))
                .addGap(25, 25, 25))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(83, 83, 83)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(jcbViviendaActual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel50)
                            .addComponent(jcbMedico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(tfNss, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel11)
                                .addComponent(jLabel12)
                                .addComponent(jcbCentroSalud, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(labelReaload)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(28, 28, 28)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(btnAceptarGeneral)
                                            .addComponent(btnEditarGeneral)
                                            .addComponent(btnCancelarGeneral)))
                                    .addComponent(btnEstablecerPasswd)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jLabel2)
                                            .addComponent(tfApellido1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel3)
                                            .addComponent(tfApellido2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel1)
                                            .addComponent(tfNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(0, 6, Short.MAX_VALUE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel4)
                                        .addComponent(tfDDni, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel5)
                                        .addComponent(tfTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jcbGenero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel8)
                                        .addComponent(jLabel10))
                                    .addComponent(tfNacimiento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel6)
                                        .addComponent(tfEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(55, 55, 55)
                                .addComponent(tfFechaAlta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );

        tabbedPaneGeneral.setName(""); // NOI18N
        tabbedPaneGeneral.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tabbedPaneGeneralKeyPressed(evt);
            }
        });

        jLabel14.setText("Vivienda:");

        btnAgregarVivienda.setText("+");
        btnAgregarVivienda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarViviendaActionPerformed(evt);
            }
        });

        jPanel7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel15.setText("Tipo de via:");

        jcbTipoVia.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Calle", "Avenida", "Cañada", "Carretera", "Jardin", "Bulevar" }));
        jcbTipoVia.setEnabled(false);
        jcbTipoVia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbTipoViaActionPerformed(evt);
            }
        });

        jLabel16.setText("Dirección:");

        tfDireccion.setEditable(false);
        tfDireccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfDireccionActionPerformed(evt);
            }
        });

        jLabel17.setText("Num:");

        tfNumero.setEditable(false);

        tfPiso.setEditable(false);

        jLabel18.setText("Piso:");

        tfLetra.setEditable(false);

        jLabel19.setText("Letra:");

        tfEscalera.setEditable(false);
        tfEscalera.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfEscaleraActionPerformed(evt);
            }
        });

        jLabel20.setText("Escalera:");

        jLabel21.setText("Ciudad-Codigo Postal");

        jLabel23.setText("Provincia:");

        jLabel24.setText("Tipo de vivenda:");

        jcbTipoVivienda.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Unifamiliar", "Piso Tutelado", "Edificio de vecinos", "Unifamilar Aislada", "Unifamilar Aislada Rural" }));
        jcbTipoVivienda.setEnabled(false);

        jLabel25.setText("Modo de acceso:");

        tfModoAcceso.setEditable(false);
        tfModoAcceso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfModoAccesoActionPerformed(evt);
            }
        });

        btnAceptarVivienda.setText("Aceptar");
        btnAceptarVivienda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarViviendaActionPerformed(evt);
            }
        });

        btnCancelaVivienda.setText("Cancelar");
        btnCancelaVivienda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelaViviendaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelProvLayout = new javax.swing.GroupLayout(panelProv);
        panelProv.setLayout(panelProvLayout);
        panelProvLayout.setHorizontalGroup(
            panelProvLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 209, Short.MAX_VALUE)
        );
        panelProvLayout.setVerticalGroup(
            panelProvLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 42, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout panelPostalLayout = new javax.swing.GroupLayout(panelPostal);
        panelPostal.setLayout(panelPostalLayout);
        panelPostalLayout.setHorizontalGroup(
            panelPostalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 302, Short.MAX_VALUE)
        );
        panelPostalLayout.setVerticalGroup(
            panelPostalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jcbTipoVivienda, javax.swing.GroupLayout.Alignment.LEADING, 0, 1, Short.MAX_VALUE)
                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel15)
                                    .addComponent(jcbTipoVia, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jLabel24))
                        .addGap(30, 30, 30)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel7Layout.createSequentialGroup()
                                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel16)
                                            .addComponent(tfDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(30, 30, 30)
                                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(tfNumero, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel17))
                                        .addGap(30, 30, 30)
                                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(tfPiso, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel18)))
                                    .addComponent(tfModoAcceso, javax.swing.GroupLayout.PREFERRED_SIZE, 305, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(30, 30, 30)
                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel7Layout.createSequentialGroup()
                                        .addComponent(btnAceptarVivienda, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnCancelaVivienda))
                                    .addGroup(jPanel7Layout.createSequentialGroup()
                                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(tfLetra, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel19))
                                        .addGap(30, 30, 30)
                                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel20)
                                            .addComponent(tfEscalera, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                            .addComponent(jLabel25)))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(panelProv, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addGap(26, 26, 26)
                                .addComponent(jLabel21))
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(panelPostal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(150, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(jLabel16)
                    .addComponent(jLabel17)
                    .addComponent(jLabel18)
                    .addComponent(jLabel19)
                    .addComponent(jLabel20))
                .addGap(18, 18, 18)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jcbTipoVia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tfDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tfNumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tfPiso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tfLetra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tfEscalera, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(38, 38, 38)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel21)
                    .addComponent(jLabel23))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(panelProv, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelPostal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel24)
                    .addComponent(jLabel25))
                .addGap(18, 18, 18)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jcbTipoVivienda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tfModoAcceso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAceptarVivienda)
                    .addComponent(btnCancelaVivienda))
                .addContainerGap(57, Short.MAX_VALUE))
        );

        jLabel26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Vista/Imagens/casa.png"))); // NOI18N

        btnEliminarVivienda.setText("-");
        btnEliminarVivienda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarViviendaActionPerformed(evt);
            }
        });

        btnEditarViviendas.setText("Editar");
        btnEditarViviendas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarViviendasActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addComponent(jcbViviendas, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnAgregarVivienda, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnEliminarVivienda, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnEditarViviendas)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 566, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel26)))
                .addGap(50, 50, 50))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(jcbViviendas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAgregarVivienda)
                    .addComponent(btnEliminarVivienda)
                    .addComponent(btnEditarViviendas))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel26))
                .addContainerGap(84, Short.MAX_VALUE))
        );

        tabbedPaneGeneral.addTab("Viviendas", jPanel2);

        jtableHistorial.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Tipo", "Decripcion"
            }
        ));
        jScrollPane6.setViewportView(jtableHistorial);

        jLabel51.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel51.setText("Registro:");

        btnNuevoRegistroMedico.setText("+");
        btnNuevoRegistroMedico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoRegistroMedicoActionPerformed(evt);
            }
        });

        btnEliminarRegistroMed.setText("-");
        btnEliminarRegistroMed.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarRegistroMedActionPerformed(evt);
            }
        });

        btnAceptarHistorial.setText("Aceptar");
        btnAceptarHistorial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarHistorialActionPerformed(evt);
            }
        });

        btnCancelarHisotial.setText("Cancelar");
        btnCancelarHisotial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarHisotialActionPerformed(evt);
            }
        });

        btnEditarHistorial.setText("Editar");
        btnEditarHistorial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarHistorialActionPerformed(evt);
            }
        });

        jLabel41.setText("Tipo:");

        jLabel48.setText("Descripción:");

        javax.swing.GroupLayout panelHistorialEditLayout = new javax.swing.GroupLayout(panelHistorialEdit);
        panelHistorialEdit.setLayout(panelHistorialEditLayout);
        panelHistorialEditLayout.setHorizontalGroup(
            panelHistorialEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelHistorialEditLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel41)
                .addGap(18, 18, 18)
                .addComponent(tfTipoHistorial, javax.swing.GroupLayout.PREFERRED_SIZE, 255, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel48)
                .addGap(18, 18, 18)
                .addComponent(tfDescripciónHistorial, javax.swing.GroupLayout.DEFAULT_SIZE, 386, Short.MAX_VALUE)
                .addContainerGap())
        );
        panelHistorialEditLayout.setVerticalGroup(
            panelHistorialEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelHistorialEditLayout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(panelHistorialEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel41)
                    .addComponent(tfTipoHistorial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel48)
                    .addComponent(tfDescripciónHistorial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(47, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout panelHLayout = new javax.swing.GroupLayout(panelH);
        panelH.setLayout(panelHLayout);
        panelHLayout.setHorizontalGroup(
            panelHLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelHLayout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addGroup(panelHLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(panelHistorialEdit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelHLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel51)
                        .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 803, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(panelHLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnEditarHistorial)
                    .addGroup(panelHLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(btnEliminarRegistroMed, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnNuevoRegistroMedico, javax.swing.GroupLayout.Alignment.LEADING))
                    .addGroup(panelHLayout.createSequentialGroup()
                        .addComponent(btnAceptarHistorial, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnCancelarHisotial, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(172, Short.MAX_VALUE))
        );
        panelHLayout.setVerticalGroup(
            panelHLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelHLayout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(jLabel51)
                .addGap(18, 18, 18)
                .addGroup(panelHLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelHLayout.createSequentialGroup()
                        .addComponent(btnNuevoRegistroMedico)
                        .addGap(18, 18, 18)
                        .addComponent(btnEliminarRegistroMed)
                        .addGap(86, 86, 86)
                        .addComponent(btnEditarHistorial)))
                .addGap(15, 15, 15)
                .addGroup(panelHLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelHLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnAceptarHistorial)
                        .addComponent(btnCancelarHisotial))
                    .addComponent(panelHistorialEdit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(104, Short.MAX_VALUE))
        );

        tabbedPaneGeneral.addTab("Historial", panelH);

        jLHoraInicio.setText("Hora:");

        jlFechaInicio.setText("Fecha inicio:");

        tfFechaInicio.setCalendarPreferredSize(new java.awt.Dimension(330, 170));
        tfFechaInicio.setNavigateFont(new java.awt.Font("Serif", java.awt.Font.PLAIN, 11));

        jLMinutoInicio.setText(":");

        jTableEstado.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane7.setViewportView(jTableEstado);

        btnAgregarEstado.setLabel("+");
        btnAgregarEstado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarEstadoActionPerformed(evt);
            }
        });

        btnEliminarEstado.setLabel("-");
        btnEliminarEstado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarEstadoActionPerformed(evt);
            }
        });

        btnAceptarEstado.setText("Aceptar");
        btnAceptarEstado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarEstadoActionPerformed(evt);
            }
        });

        btnCancelarEstado.setText("Cancelar");
        btnCancelarEstado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarEstadoActionPerformed(evt);
            }
        });

        jLFechaFin.setText("Fecha fin:");

        tfFechaFin.setCalendarPreferredSize(new java.awt.Dimension(350, 200));
        tfFechaFin.setNavigateFont(new java.awt.Font("Serif", java.awt.Font.PLAIN, 12));

        jLHoraFin.setText("Hora:");

        jLMinutoFin.setText(":");

        jLdescripcionEstado.setText("Descripción:");

        btnEditarEstado.setText("Editar");
        btnEditarEstado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarEstadoActionPerformed(evt);
            }
        });

        tfDescripcionEstado.setColumns(20);
        tfDescripcionEstado.setRows(5);
        tfDescripcionEstado.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jScrollPane8.setViewportView(tfDescripcionEstado);

        javax.swing.GroupLayout panelDescripcionLayout = new javax.swing.GroupLayout(panelDescripcion);
        panelDescripcion.setLayout(panelDescripcionLayout);
        panelDescripcionLayout.setHorizontalGroup(
            panelDescripcionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 405, Short.MAX_VALUE)
        );
        panelDescripcionLayout.setVerticalGroup(
            panelDescripcionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
        );

        jLabel53.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel53.setText("Estados del paciente:");

        javax.swing.GroupLayout panelEstadoLayout = new javax.swing.GroupLayout(panelEstado);
        panelEstado.setLayout(panelEstadoLayout);
        panelEstadoLayout.setHorizontalGroup(
            panelEstadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelEstadoLayout.createSequentialGroup()
                .addGroup(panelEstadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelEstadoLayout.createSequentialGroup()
                        .addGap(130, 130, 130)
                        .addGroup(panelEstadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(panelEstadoLayout.createSequentialGroup()
                                .addComponent(btnAceptarEstado)
                                .addGap(18, 18, 18)
                                .addComponent(btnCancelarEstado))
                            .addGroup(panelEstadoLayout.createSequentialGroup()
                                .addGroup(panelEstadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLFechaFin, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jlFechaInicio))
                                .addGap(18, 18, 18)
                                .addGroup(panelEstadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(panelEstadoLayout.createSequentialGroup()
                                        .addComponent(tfFechaInicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLHoraInicio)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(tfHoraInicio, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLMinutoInicio)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(tfMinutoInicio, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(28, 28, 28)
                                        .addComponent(jLdescripcionEstado))
                                    .addGroup(panelEstadoLayout.createSequentialGroup()
                                        .addComponent(tfFechaFin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLHoraFin)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(tfHoraFin, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLMinutoFin)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(tfMinutoFin, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(18, 18, 18)
                                .addComponent(panelDescripcion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(panelEstadoLayout.createSequentialGroup()
                        .addGap(93, 93, 93)
                        .addGroup(panelEstadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel53)
                            .addGroup(panelEstadoLayout.createSequentialGroup()
                                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 988, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(panelEstadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btnEditarEstado)
                                    .addComponent(btnAgregarEstado)
                                    .addComponent(btnEliminarEstado))))))
                .addContainerGap(154, Short.MAX_VALUE))
        );
        panelEstadoLayout.setVerticalGroup(
            panelEstadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelEstadoLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnAgregarEstado)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnEliminarEstado)
                .addGap(18, 18, 18)
                .addComponent(btnEditarEstado)
                .addGap(317, 317, 317))
            .addGroup(panelEstadoLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(jLabel53)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 38, Short.MAX_VALUE)
                .addGroup(panelEstadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelEstadoLayout.createSequentialGroup()
                        .addGroup(panelEstadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(panelEstadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(tfFechaInicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(panelEstadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLHoraInicio)
                                    .addComponent(tfHoraInicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(jlFechaInicio))
                            .addGroup(panelEstadoLayout.createSequentialGroup()
                                .addComponent(jLMinutoInicio)
                                .addGap(8, 8, 8))
                            .addComponent(tfMinutoInicio, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(32, 32, 32)
                        .addGroup(panelEstadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLFechaFin)
                            .addComponent(tfFechaFin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(panelEstadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLMinutoFin)
                                .addComponent(tfMinutoFin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(tfHoraFin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLHoraFin))))
                    .addComponent(jLdescripcionEstado)
                    .addComponent(panelDescripcion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panelEstadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAceptarEstado)
                    .addComponent(btnCancelarEstado))
                .addGap(28, 28, 28))
        );

        tabbedPaneGeneral.addTab("Estado", panelEstado);

        jcbContactos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbContactosActionPerformed(evt);
            }
        });

        jPanel8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel8.setAutoscrolls(true);

        jLabel29.setText("DNI:");

        tfDniFamiliar.setEditable(false);
        tfDniFamiliar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfDniFamiliarActionPerformed(evt);
            }
        });

        jLabel30.setText("1º Apellido:");

        tfApellido1Familiar.setEditable(false);
        tfApellido1Familiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfApellido1FamiliarActionPerformed(evt);
            }
        });

        jLabel31.setText("2º Apellido:");

        tfApellido2Familiar.setEditable(false);
        tfApellido2Familiar.setToolTipText("");
        tfApellido2Familiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfApellido2FamiliarActionPerformed(evt);
            }
        });

        jLabel32.setText("Nombre:");

        tfNombreFamiliar.setEditable(false);

        jLabel33.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel33.setText("Teléfono:");

        tfTelefonoFamilia.setEditable(false);
        tfTelefonoFamilia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfTelefonoFamiliaActionPerformed(evt);
            }
        });

        cbTieneLlaves.setText("Tiene llaves");
        cbTieneLlaves.setEnabled(false);

        jLabel34.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel34.setText("Email:");

        tfEmailFamiliar.setEditable(false);
        tfEmailFamiliar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfEmailFamiliarActionPerformed(evt);
            }
        });

        jLabel35.setText("Disponibilidad:");

        tfDisponibilidadfamiliar.setEditable(false);

        jLabel36.setText("Observaciones:");

        tfObreservaciones.setEditable(false);
        tfObreservaciones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfObreservacionesActionPerformed(evt);
            }
        });

        jLabel38.setText("Tipo de via:");

        jcbTipoViaFamiliar.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Calle", "Avenida", "Cañada", "Carretera", "Jardin", "Bulevar" }));
        jcbTipoViaFamiliar.setSelectedItem(null);
        jcbTipoViaFamiliar.setEnabled(false);

        tfDireccionFamiliar.setEditable(false);
        tfDireccionFamiliar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfDireccionFamiliarActionPerformed(evt);
            }
        });

        jLabel42.setText("Dirección:");

        jLabel43.setText("Num:");

        tfNumeroFamiliar.setEditable(false);

        tfPisoFamiliar.setEditable(false);

        jLabel44.setText("Piso:");

        tfLetraFamiliar.setEditable(false);

        jLabel45.setText("Letra:");

        jLabel46.setText("Escalera:");

        tfEscaleraFamiliar.setEditable(false);
        tfEscaleraFamiliar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfEscaleraFamiliarActionPerformed(evt);
            }
        });

        btnAceptarFamiliar.setText("Aceptar");
        btnAceptarFamiliar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarFamiliarActionPerformed(evt);
            }
        });

        btnCancelarFamiliar.setText("Cancelar");
        btnCancelarFamiliar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarFamiliarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelProvFamiliaLayout = new javax.swing.GroupLayout(panelProvFamilia);
        panelProvFamilia.setLayout(panelProvFamiliaLayout);
        panelProvFamiliaLayout.setHorizontalGroup(
            panelProvFamiliaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 209, Short.MAX_VALUE)
        );
        panelProvFamiliaLayout.setVerticalGroup(
            panelProvFamiliaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jLabel39.setText("Provincia:");

        jLabel22.setText("Ciudad-Codigo Postal");

        javax.swing.GroupLayout panelPostalFamiliaLayout = new javax.swing.GroupLayout(panelPostalFamilia);
        panelPostalFamilia.setLayout(panelPostalFamiliaLayout);
        panelPostalFamiliaLayout.setHorizontalGroup(
            panelPostalFamiliaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 272, Short.MAX_VALUE)
        );
        panelPostalFamiliaLayout.setVerticalGroup(
            panelPostalFamiliaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 42, Short.MAX_VALUE)
        );

        jLabel28.setText("Relación:");

        tfRelación.setEditable(false);

        jLabel27.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel27.setText("•Datos de la vivienda:");

        jLabel52.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel52.setText("•Datos personales:");

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel8Layout.createSequentialGroup()
                                .addComponent(jLabel36)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(tfObreservaciones, javax.swing.GroupLayout.PREFERRED_SIZE, 349, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel28)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tfRelación, javax.swing.GroupLayout.PREFERRED_SIZE, 257, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(jPanel8Layout.createSequentialGroup()
                                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel8Layout.createSequentialGroup()
                                        .addGap(3, 3, 3)
                                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel32)
                                            .addComponent(jLabel29))
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(tfDniFamiliar, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(tfNombreFamiliar, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel30)
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(tfTelefonoFamilia, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(tfApellido1Familiar, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(jPanel8Layout.createSequentialGroup()
                                        .addGap(237, 237, 237)
                                        .addComponent(jLabel33, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                                        .addComponent(jLabel35)
                                        .addGap(18, 18, 18)
                                        .addComponent(tfDisponibilidadfamiliar, javax.swing.GroupLayout.PREFERRED_SIZE, 374, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel8Layout.createSequentialGroup()
                                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(jLabel31, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jLabel34, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel8Layout.createSequentialGroup()
                                                .addGap(0, 0, Short.MAX_VALUE)
                                                .addComponent(tfEmailFamiliar, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(jPanel8Layout.createSequentialGroup()
                                                .addComponent(tfApellido2Familiar, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, Short.MAX_VALUE))))
                                    .addGroup(jPanel8Layout.createSequentialGroup()
                                        .addComponent(cbTieneLlaves)
                                        .addGap(0, 0, Short.MAX_VALUE)))))
                        .addGap(269, 269, 269))
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel8Layout.createSequentialGroup()
                                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel39, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(panelProvFamilia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel8Layout.createSequentialGroup()
                                        .addGap(26, 26, 26)
                                        .addComponent(jLabel22))
                                    .addGroup(jPanel8Layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(panelPostalFamilia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(26, 26, 26)
                                        .addComponent(btnAceptarFamiliar, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnCancelarFamiliar, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(jPanel8Layout.createSequentialGroup()
                                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel38)
                                    .addComponent(jcbTipoViaFamiliar, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(30, 30, 30)
                                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel42)
                                    .addComponent(tfDireccionFamiliar, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(30, 30, 30)
                                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(tfNumeroFamiliar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel43))
                                .addGap(30, 30, 30)
                                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(tfPisoFamiliar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel44))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(tfLetraFamiliar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel45))
                                .addGap(30, 30, 30)
                                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel46)
                                    .addComponent(tfEscaleraFamiliar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jLabel27)
                            .addComponent(jLabel52))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addComponent(jLabel52)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 11, Short.MAX_VALUE)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel31, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel29)
                        .addComponent(tfDniFamiliar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel30)
                        .addComponent(tfApellido1Familiar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(tfApellido2Familiar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel34)
                    .addComponent(jLabel33)
                    .addComponent(tfTelefonoFamilia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel32)
                    .addComponent(tfNombreFamiliar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tfEmailFamiliar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel35)
                    .addComponent(tfDisponibilidadfamiliar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbTieneLlaves))
                .addGap(18, 18, 18)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel36)
                    .addComponent(tfObreservaciones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel28)
                    .addComponent(tfRelación, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel27)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel38)
                            .addComponent(jLabel42)
                            .addComponent(jLabel43)
                            .addComponent(jLabel44))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jcbTipoViaFamiliar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tfDireccionFamiliar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tfNumeroFamiliar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tfPisoFamiliar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel45)
                            .addComponent(jLabel46))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(tfLetraFamiliar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tfEscaleraFamiliar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(24, 24, 24)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel22)
                            .addComponent(jLabel39))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(panelProvFamilia, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(panelPostalFamilia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnAceptarFamiliar)
                        .addComponent(btnCancelarFamiliar)))
                .addContainerGap())
        );

        btnAgregarFamiliar.setText("+");
        btnAgregarFamiliar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarFamiliarActionPerformed(evt);
            }
        });

        btnEliminarFamiliar.setText("-");
        btnEliminarFamiliar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarFamiliarActionPerformed(evt);
            }
        });

        btnEditarFamiliar.setText("Editar");
        btnEditarFamiliar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarFamiliarActionPerformed(evt);
            }
        });

        jlistFamiliares.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane9.setViewportView(jlistFamiliares);

        btnArriba.setText("↑");
        btnArriba.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnArribaActionPerformed(evt);
            }
        });

        btnAbajo.setText("↓");
        btnAbajo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAbajoActionPerformed(evt);
            }
        });

        jLabel13.setText("Prioridad:");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, 892, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addComponent(jcbContactos, javax.swing.GroupLayout.PREFERRED_SIZE, 331, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnAgregarFamiliar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnEliminarFamiliar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnEditarFamiliar)))
                .addGap(47, 47, 47)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane9, javax.swing.GroupLayout.DEFAULT_SIZE, 224, Short.MAX_VALUE)
                    .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnArriba, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnAbajo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(44, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jcbContactos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAgregarFamiliar)
                    .addComponent(btnEliminarFamiliar)
                    .addComponent(btnEditarFamiliar)
                    .addComponent(jLabel13))
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, 372, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(151, 151, 151)
                        .addComponent(btnArriba)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnAbajo)))
                .addContainerGap(48, Short.MAX_VALUE))
        );

        tabbedPaneGeneral.addTab("Personas de contacto", jPanel4);

        jLabel37.setText("Notificaciones:");

        jlistNotificaciones.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane2.setViewportView(jlistNotificaciones);

        jLabel47.setText("Nombre:");

        tfNombreNotificacion.setEditable(false);

        jLabel49.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel49.setText("Tipo:");

        jcbTipoNotificacion.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Medicamento", "Llamada", "Notificacion" }));
        jcbTipoNotificacion.setEnabled(false);
        jcbTipoNotificacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbTipoNotificacionActionPerformed(evt);
            }
        });

        labelFechaInicio.setText("Fecha inicio:");

        labelFechaFin.setText("Fecha fin:");

        tfFechaInicioNot.setCalendarPreferredSize(new java.awt.Dimension(350, 200));
        tfFechaInicioNot.setNothingAllowed(false);
        tfFechaInicioNot.setEnabled(false);
        tfFechaInicioNot.setNavigateFont(new java.awt.Font("Serif", java.awt.Font.PLAIN, 12));

        tfFechaFinNot.setCalendarPreferredSize(new java.awt.Dimension(350, 200));
        tfFechaFinNot.setEnabled(false);

        jTableNotifiaciones.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane5.setViewportView(jTableNotifiaciones);

        bntNuevaNotificacion.setText("+");
        bntNuevaNotificacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bntNuevaNotificacionActionPerformed(evt);
            }
        });

        btnBorrarNotificacion.setText("-");
        btnBorrarNotificacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBorrarNotificacionActionPerformed(evt);
            }
        });

        btnAgregarRocordatorio.setText("+");
        btnAgregarRocordatorio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarRocordatorioActionPerformed(evt);
            }
        });

        btnEliminarRecordatorio.setText("-");
        btnEliminarRecordatorio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarRecordatorioActionPerformed(evt);
            }
        });

        btnAceparNotificacion.setText("Aceptar");
        btnAceparNotificacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceparNotificacionActionPerformed(evt);
            }
        });

        btnCancelarNotificacion.setText("Cancelar");
        btnCancelarNotificacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarNotificacionActionPerformed(evt);
            }
        });

        btnEditarNotificacion.setText("Editar");
        btnEditarNotificacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarNotificacionActionPerformed(evt);
            }
        });

        btnEditarRecordatorio.setText("Editar");
        btnEditarRecordatorio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarRecordatorioActionPerformed(evt);
            }
        });

        jLMinutoInicio1.setText(":");

        jLHoraInicio1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLHoraInicio1.setText("Hora:");

        jLabel40.setText("Descripción:");

        tfDescripcionNotificacion.setColumns(20);
        tfDescripcionNotificacion.setRows(2);
        jScrollPane3.setViewportView(tfDescripcionNotificacion);

        javax.swing.GroupLayout panelRecordatorioLayout = new javax.swing.GroupLayout(panelRecordatorio);
        panelRecordatorio.setLayout(panelRecordatorioLayout);
        panelRecordatorioLayout.setHorizontalGroup(
            panelRecordatorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelRecordatorioLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelRecordatorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelRecordatorioLayout.createSequentialGroup()
                        .addComponent(jLabel40)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane3))
                    .addGroup(panelRecordatorioLayout.createSequentialGroup()
                        .addComponent(jLHoraInicio1, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(9, 9, 9)
                        .addComponent(tfHoraInicioNot, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLMinutoInicio1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tfMinutoInicioNot, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panelRecordatorioLayout.setVerticalGroup(
            panelRecordatorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelRecordatorioLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelRecordatorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panelRecordatorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLHoraInicio1)
                        .addComponent(tfHoraInicioNot, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelRecordatorioLayout.createSequentialGroup()
                        .addComponent(jLMinutoInicio1)
                        .addGap(8, 8, 8))
                    .addComponent(tfMinutoInicioNot, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                .addGroup(panelRecordatorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel40, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19))
        );

        btnAceptarRecordatorio.setText("Aceptar");
        btnAceptarRecordatorio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarRecordatorioActionPerformed(evt);
            }
        });

        btnCancelarRecordatorio.setText("Cancelar");
        btnCancelarRecordatorio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarRecordatorioActionPerformed(evt);
            }
        });

        cbFechaFin.setText("Sin fecha fin");
        cbFechaFin.setToolTipText("");
        cbFechaFin.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                cbFechaFinStateChanged(evt);
            }
        });
        cbFechaFin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbFechaFinActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel37)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnEditarNotificacion)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(btnBorrarNotificacion, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(bntNuevaNotificacion, javax.swing.GroupLayout.Alignment.LEADING)))))
                .addGap(49, 49, 49)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(labelFechaInicio)
                        .addGap(18, 18, 18)
                        .addComponent(tfFechaInicioNot, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(labelFechaFin)
                        .addGap(18, 18, 18)
                        .addComponent(tfFechaFinNot, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cbFechaFin))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel47)
                        .addGap(18, 18, 18)
                        .addComponent(tfNombreNotificacion, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel49)
                        .addGap(18, 18, 18)
                        .addComponent(jcbTipoNotificacion, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 522, Short.MAX_VALUE)
                            .addComponent(panelRecordatorio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(btnAceptarRecordatorio, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnAceparNotificacion, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnEliminarRecordatorio, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnAgregarRocordatorio, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(btnCancelarNotificacion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnCancelarRecordatorio, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(btnEditarRecordatorio))))
                .addContainerGap(129, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel47)
                                    .addComponent(tfNombreNotificacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel49)
                                    .addComponent(jcbTipoNotificacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(labelFechaFin, javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(tfFechaFinNot, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(labelFechaInicio))
                                    .addComponent(tfFechaInicioNot, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(cbFechaFin))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                                .addComponent(btnAgregarRocordatorio)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnEliminarRecordatorio)
                                .addGap(18, 18, 18)
                                .addComponent(btnEditarRecordatorio)
                                .addGap(62, 62, 62)))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(btnAceparNotificacion)
                                    .addComponent(btnCancelarNotificacion))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(btnAceptarRecordatorio)
                                    .addComponent(btnCancelarRecordatorio)))
                            .addComponent(panelRecordatorio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel37)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(bntNuevaNotificacion)
                                .addGap(18, 18, 18)
                                .addComponent(btnBorrarNotificacion)
                                .addGap(18, 18, 18)
                                .addComponent(btnEditarNotificacion)))))
                .addContainerGap(48, Short.MAX_VALUE))
        );

        tabbedPaneGeneral.addTab("Notificaciones", jPanel3);

        btnSeguimiento.setText("Seguimiento");
        btnSeguimiento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSeguimientoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 154, Short.MAX_VALUE)
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 73, Short.MAX_VALUE)
        );

        btnActualizarMapa.setText("Actualizar");
        btnActualizarMapa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarMapaActionPerformed(evt);
            }
        });

        labelMapa.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        labelMapa.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        btnZOOMMas.setText("ZOOM+");
        btnZOOMMas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnZOOMMasActionPerformed(evt);
            }
        });

        btnZOOMMenos.setText("ZOOM-");
        btnZOOMMenos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnZOOMMenosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap(214, Short.MAX_VALUE)
                .addComponent(labelMapa, javax.swing.GroupLayout.PREFERRED_SIZE, 641, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(139, 139, 139)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnZOOMMas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnZOOMMenos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 58, Short.MAX_VALUE)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(btnActualizarMapa, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnSeguimiento, javax.swing.GroupLayout.Alignment.LEADING))))
                .addGap(24, 24, 24))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(45, 45, 45)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnActualizarMapa)
                            .addComponent(btnZOOMMas))
                        .addGap(27, 27, 27)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnZOOMMenos)
                            .addComponent(btnSeguimiento)))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(43, 43, 43)
                        .addComponent(labelMapa, javax.swing.GroupLayout.PREFERRED_SIZE, 358, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(87, Short.MAX_VALUE))
        );

        tabbedPaneGeneral.addTab("Geolocalización", jPanel6);

        jTableLlamadas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTableLlamadas.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jScrollPane1.setViewportView(jTableLlamadas);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(176, 176, 176)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 937, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(166, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(75, 75, 75)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 302, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(111, Short.MAX_VALUE))
        );

        tabbedPaneGeneral.addTab("Lllamadas", jPanel5);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(tabbedPaneGeneral, javax.swing.GroupLayout.PREFERRED_SIZE, 1281, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 6, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(tabbedPaneGeneral)
                .addContainerGap())
        );

        tabbedPaneGeneral.getAccessibleContext().setAccessibleName("tabbedPane");
        tabbedPaneGeneral.getAccessibleContext().setAccessibleDescription("");
    }// </editor-fold>//GEN-END:initComponents

    private void tfApellido1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfApellido1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfApellido1ActionPerformed

    private void tfNssActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfNssActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfNssActionPerformed


    private void btnAceptarViviendaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarViviendaActionPerformed
        ViviendaTable vivienda;
        DireccionTable direccion;
        if (validarVivienda()) {
            if (nuevoRegistro) {
                vivienda = new ViviendaTable();
                direccion = new DireccionTable();
                vivienda.setDireccionTable(direccion);
            } else {
                vivienda = (ViviendaTable) jcbViviendas.getSelectedItem();
                direccion = vivienda.getDireccionTable();
            }

            direccion.setTipoVia(jcbTipoVia.getSelectedItem().toString());
            direccion.setDireccion(tfDireccion.getText());
            direccion.setNum(Integer.valueOf(tfNumero.getText()));
            if (!tfPiso.getText().equals("")) {
                direccion.setPiso(Integer.valueOf(tfPiso.getText()));
            }
            if (!tfLetra.getText().equals("")) {
                direccion.setLetra(tfLetra.getText());
            }
            if (!tfEscalera.getText().equals("")) {
                direccion.setEscalera(tfEscalera.getText());
            }

            direccion.setCiudadTable((CiudadTable) jcbCiudad.getSelectedItem());

            vivienda.setTipo(jcbTipoVivienda.getSelectedItem().toString());
            vivienda.setModoAcceso(tfModoAcceso.getText());
            vivienda.setDependienteTable(dependiente);

            if (nuevoRegistro) {
                bd.insert(direccion);
                bd.insert(vivienda);

                jcbViviendaActual.addItem(vivienda);
                jcbViviendas.addItem(vivienda);
                jcbViviendas.setSelectedItem(vivienda);
                cargarDatosVivienda();
                if (dependiente != null) {
                    nuevoRegistro = false;

                }

            } else {

                bd.update(direccion);
                bd.update(vivienda);

                bd.consultarViviendas(jcbViviendaActual, dependiente.getId());
                jcbViviendaActual.setSelectedItem(dependiente.getViviendaTable());
            }
            cambiarEstadoViviendas(false);
            btnEditarGeneral.setEnabled(true);

        }
    }//GEN-LAST:event_btnAceptarViviendaActionPerformed

    private void tfModoAccesoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfModoAccesoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfModoAccesoActionPerformed

    private void tfEscaleraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfEscaleraActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfEscaleraActionPerformed

    private void tfDireccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfDireccionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfDireccionActionPerformed

    private void jcbTipoViaActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
    }

    private void btnAgregarViviendaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarViviendaActionPerformed
        jcbTipoVia.setSelectedIndex(-1);
        tfDireccion.setText("");
        tfNumero.setText("");
        tfPiso.setText("");
        tfLetra.setText("");
        tfEscalera.setText("");
        jcbProvincia.setSelectedIndex(-1);
        jcbCiudad.setSelectedIndex(-1);
        jcbTipoVivienda.setSelectedIndex(-1);
        tfModoAcceso.setText("");
        nuevoRegistro = true;

        cambiarEstadoViviendas(true);
    }//GEN-LAST:event_btnAgregarViviendaActionPerformed

    private void btnZOOMMasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnZOOMMasActionPerformed

        try {

            if (mapa == null) {

                initMaps();

            } else {

                mapa.Acercar();
                labelMapa.setIcon(mapa.getImagen());

            }
        } catch (IOException ex) {
            Logger.getLogger(SeguimientoGeolocalizacion.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_btnZOOMMasActionPerformed

    private void btnZOOMMenosActionPerformed(java.awt.event.ActionEvent evt) {

        try {

            if (mapa == null) {

                initMaps();

            } else {
                mapa.Alejar();
                labelMapa.setIcon(mapa.getImagen());
            }
        } catch (IOException ex) {
            Logger.getLogger(SeguimientoGeolocalizacion.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    private void tabbedPaneGeneralKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tabbedPaneGeneralKeyPressed

    }//GEN-LAST:event_tabbedPaneGeneralKeyPressed

    private void btnAceptarGeneralActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarGeneralActionPerformed
        PersonaTable persona;
        if (validarPersona()) {
            if (nuevoRegistro) {
                dependiente = new DependienteTable();
                persona = new PersonaTable();
                tabbedPaneGeneral.setEnabled(true);
                btnAgregarVivienda.setVisible(true);
                btnEliminarVivienda.setVisible(true);

            } else {
                persona = dependiente.getPersonaTable();

            }

            persona.setDni(tfDDni.getText());
            persona.setApellido1(tfApellido1.getText());
            persona.setApellido2(tfApellido2.getText());
            persona.setNombre(tfNombre.getText());
            persona.setTelefono(tfTelefono.getText());
            persona.setEmail(tfEmail.getText());

            dependiente.setGenero(jcbGenero.getSelectedItem().toString());
            dependiente.setFecNacimiento(tfNacimiento.getSelectedDate().getTime());
            dependiente.setFecAlta(tfFechaAlta.getSelectedDate().getTime());
            dependiente.setNss(tfNss.getText());
            dependiente.setViviendaTable((ViviendaTable) jcbViviendaActual.getSelectedItem());
            dependiente.setMedicoTable((MedicoTable) jcbMedico.getSelectedItem());
            dependiente.setCentrosaludTable((CentrosaludTable) jcbCentroSalud.getSelectedItem());

            if (nuevoRegistro) {
                dependiente.setPassword(vistaPassword.getPasswd());
                bd.insert(persona);
                dependiente.setPersonaTable(persona);
                bd.insert(dependiente);
                nuevoRegistro = false;
                vista.cambiarNombrePestaña(this);

                dependiente.getViviendaTable().setDependienteTable(dependiente);
                bd.update(dependiente.getViviendaTable());
                btnAgregarVivienda.setEnabled(true);
                btnEliminarVivienda.setEnabled(true);
                btnEditarViviendas.setEnabled(true);
                jcbViviendas.setEnabled(true);

                //
            } else {
                if (vistaPassword != null && !vistaPassword.getPasswd().equals("")) {
                    dependiente.setPassword(vistaPassword.getPasswd());

                }
                bd.update(persona);
                bd.update(dependiente);
            }
            cambiarEstadoGeneral(false);

        }

    }//GEN-LAST:event_btnAceptarGeneralActionPerformed

    private void btnEditarGeneralActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarGeneralActionPerformed
        cambiarEstadoGeneral(true);

    }//GEN-LAST:event_btnEditarGeneralActionPerformed

    private void btnCancelarGeneralActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarGeneralActionPerformed
        cargarDatosDependiente();
        cambiarEstadoGeneral(false);

    }//GEN-LAST:event_btnCancelarGeneralActionPerformed

    private void btnEditarViviendasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarViviendasActionPerformed
        cambiarEstadoViviendas(true);

    }//GEN-LAST:event_btnEditarViviendasActionPerformed

    private void btnCancelaViviendaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelaViviendaActionPerformed
        cambiarEstadoViviendas(false);
        cargarDatosVivienda();

    }//GEN-LAST:event_btnCancelaViviendaActionPerformed

    private void btnNuevoRegistroMedicoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoRegistroMedicoActionPerformed

        cambiarEstadoHistorial(true);
        nuevoRegistro = true;
        tfTipoHistorial.setText("");
        tfDescripciónHistorial.setText("");


    }//GEN-LAST:event_btnNuevoRegistroMedicoActionPerformed

    private void btnAgregarFamiliarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarFamiliarActionPerformed
        tfDniFamiliar.setText("");
        tfApellido1Familiar.setText("");
        tfApellido2Familiar.setText("");
        tfNombreFamiliar.setText("");
        tfTelefonoFamilia.setText("");
        tfEmailFamiliar.setText("");
        cbTieneLlaves.setSelected(false);
        tfDisponibilidadfamiliar.setText("");
        tfObreservaciones.setText("");

        jcbTipoViaFamiliar.setSelectedItem(-1);
        tfDireccionFamiliar.setText("");
        tfNumeroFamiliar.setText("");
        tfPisoFamiliar.setText("");
        tfLetraFamiliar.setText("");
        tfEscaleraFamiliar.setText("");
        tfRelación.setText("");

        jcbProvinciaFamiliar.setSelectedIndex(-1);
        cambiarEstadoFamiliar(true);
        nuevoRegistro = true;
    }//GEN-LAST:event_btnAgregarFamiliarActionPerformed
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed

        ArrayList<GeolocalizacionTable> geo = bd.devolverUltimaCoordenada(ID);
        ponerMapa(geo.get(geo.size() - 1).getLatitud(), geo.get(geo.size() - 1).getLongitud());

    }//GEN-LAST:event_jButton2ActionPerformed

    private void btnEliminarRegistroMedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarRegistroMedActionPerformed
        if (jtableHistorial.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(this, "Debes seleccionar algún registro para borrarlo");
        } else {
            int i = JOptionPane.showConfirmDialog(panelPostal, "Estas seguro de que deseas borrar este contacto?");
            if (i == JOptionPane.YES_OPTION) {
                HistMedicoTable hist = tableHistorial.getObject(jtableHistorial.getSelectedRow());
                tableHistorial.removeObject(hist);
                bd.remove(hist);
            }

        }

    }//GEN-LAST:event_btnEliminarRegistroMedActionPerformed

    private void btnCancelarHisotialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarHisotialActionPerformed
        cambiarEstadoHistorial(false);

        nuevoRegistro = false;

    }//GEN-LAST:event_btnCancelarHisotialActionPerformed

    private void btnAceptarHistorialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarHistorialActionPerformed

        if (validarRegistro()) {
            HistMedicoTable hist;
            if (nuevoRegistro) {
                hist = new HistMedicoTable();
                hist.setDependienteTable(dependiente);

            } else {

                hist = historialTmp;
            }

            hist.setDescripcion(tfDescripciónHistorial.getText());
            hist.setTipo(tfTipoHistorial.getText());

            if (nuevoRegistro) {
                bd.insert(hist);
                tableHistorial.addRow(hist);

            } else {
                bd.update(hist);
            }
            nuevoRegistro = false;
            cambiarEstadoHistorial(false);
        }

    }//GEN-LAST:event_btnAceptarHistorialActionPerformed

    private void btnEditarHistorialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarHistorialActionPerformed
        if (jtableHistorial.getSelectedRow() != -1) {
            historialTmp = tableHistorial.getObject(jtableHistorial.getSelectedRow());
            tfTipoHistorial.setText(historialTmp.getTipo());
            tfDescripciónHistorial.setText(historialTmp.getDescripcion());
            cambiarEstadoHistorial(true);
        } else {
            JOptionPane.showMessageDialog(this, "Debes seleccionar algún registro para editarlo. ");
        }


    }//GEN-LAST:event_btnEditarHistorialActionPerformed

    private void btnEditarFamiliarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarFamiliarActionPerformed
        cambiarEstadoFamiliar(true);

    }//GEN-LAST:event_btnEditarFamiliarActionPerformed

    private void btnCancelarFamiliarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarFamiliarActionPerformed
        nuevoRegistro = false;
        cargarDatosContactos();
        cambiarEstadoFamiliar(false);


    }//GEN-LAST:event_btnCancelarFamiliarActionPerformed

    private void tfApellido2FamiliarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfApellido2FamiliarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfApellido2FamiliarActionPerformed

    private void jcbContactosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbContactosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jcbContactosActionPerformed

    private void btnEliminarFamiliarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarFamiliarActionPerformed
        if (jcbContactos.getItemCount() == 0) {
            JOptionPane.showMessageDialog(this, "No se han encotrado contactos para borrar.");
        } else {
            int i = JOptionPane.showConfirmDialog(panelPostal, "Estas seguro de que deseas borrar este contacto?");
            if (i == JOptionPane.YES_OPTION) {
                ContactoTable contacto = (ContactoTable) jcbContactos.getModel().getSelectedItem();
                listFamiliares.removeElement(contacto);
                jcbContactos.removeItem(contacto);
                bd.remove(contacto);
                for (int x = 0; x < listFamiliares.getDatos().size(); x++) {
                    bd.update(listFamiliares.getElement(x));
                }

            }
        }

    }//GEN-LAST:event_btnEliminarFamiliarActionPerformed

    private void btnEditarEstadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarEstadoActionPerformed
        if (jTableEstado.getSelectedRow() != -1) {
            cambiarEstadoEstado(true);
            estadoTmp = tableEstado.getObject(jTableEstado.getSelectedRow());

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(estadoTmp.getFechaHora());   // assigns calendar to given date
            tfFechaFin.setSelectedDate(calendar);
            tfHoraFin.setText(String.valueOf(calendar.get(Calendar.HOUR)));
            tfMinutoFin.setText(String.valueOf(Calendar.MINUTE));

            calendar.setTime(estadoTmp.getFechaHoraInicio());   // assigns calendar to given date
            tfFechaInicio.setSelectedDate(calendar);
            tfHoraInicio.setText(String.valueOf(calendar.get(Calendar.HOUR)));
            tfMinutoInicio.setText(String.valueOf(Calendar.MINUTE));
            tfDescripcionEstado.setText(estadoTmp.getDescripcion());

        } else {
            JOptionPane.showMessageDialog(this, "Debes seleccionar un estado para editarlo");
        }


    }//GEN-LAST:event_btnEditarEstadoActionPerformed

    private void btnAgregarEstadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarEstadoActionPerformed

        cambiarEstadoEstado(true);

        nuevoRegistro = true;
        Calendar c = Calendar.getInstance();
        tfFechaInicio.setCurrent(c);
        tfHoraInicio.setText("00");
        tfMinutoInicio.setText("00");

        tfFechaFin.setCurrent(c);
        tfMinutoFin.setText("00");
        tfHoraFin.setText("00");

        tfDescripcionEstado.setText("");
    }//GEN-LAST:event_btnAgregarEstadoActionPerformed

    private void btnEliminarEstadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarEstadoActionPerformed
        if (jTableEstado.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(this, "Debes seleccionar un estado para borrarlo.");
        } else {
            int i = JOptionPane.showConfirmDialog(this, "Estas seguro de que deseas borrar este regsitro?");

            if (i == JOptionPane.YES_OPTION) {
                EstadoTable estado = tableEstado.getObject(jTableEstado.getSelectedRow());
                tableEstado.removeObject(estado);
                bd.remove(estado);
            }
        }


    }//GEN-LAST:event_btnEliminarEstadoActionPerformed

    private void btnAceptarEstadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarEstadoActionPerformed
        if (validarEstado()) {
            EstadoTable estado;
            if (nuevoRegistro) {
                estado = new EstadoTable();
            } else {
                estado = estadoTmp;

            }

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(tfFechaInicio.getSelectedDate().getTime());
            calendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(tfHoraInicio.getText()));
            calendar.set(Calendar.MINUTE, Integer.valueOf(tfMinutoInicio.getText()));
            estado.setFechaHoraInicio(calendar.getTime());

            calendar.setTime(tfFechaFin.getSelectedDate().getTime());
            calendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(tfHoraFin.getText()));
            calendar.set(Calendar.MINUTE, Integer.valueOf(tfMinutoFin.getText()));
            estado.setFechaHora(calendar.getTime());

            estado.setDescripcion(tfDescripcionEstado.getText());
            estado.setDependienteTable(dependiente);

            if (nuevoRegistro) {
                bd.insert(estado);
                tableEstado.addRow(estado);
                nuevoRegistro = false;
            } else {
                bd.update(estado);

            }
            cambiarEstadoEstado(false);

        }
    }//GEN-LAST:event_btnAceptarEstadoActionPerformed

    private void btnCancelarEstadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarEstadoActionPerformed
        cambiarEstadoEstado(false);
        nuevoRegistro = false;
    }//GEN-LAST:event_btnCancelarEstadoActionPerformed

    private void btnAgregarRocordatorioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarRocordatorioActionPerformed

        if (jlistNotificaciones.getSelectedIndex() == -1) {
            JOptionPane.showMessageDialog(this, "Debes seleccionar una notificacion para añadirle un recordatorio:");
        } else {
            nuevoRegistro = true;
            avisoTmp = listNotificaciones.getElement(jlistNotificaciones.getSelectedIndex());
            panelRecordatorio.setVisible(true);
            cambiarEstadoRecordatorio(true);
            tfHoraInicioNot.setText("00");
            tfMinutoInicioNot.setText("00");
            tfDescripcionNotificacion.setText("");
        }

    }//GEN-LAST:event_btnAgregarRocordatorioActionPerformed

    private void btnCancelarNotificacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarNotificacionActionPerformed
        cambiarEstadoNotificacion(false);
        nuevoRegistro = false;
        cargarDatosAviso();
    }//GEN-LAST:event_btnCancelarNotificacionActionPerformed

    private void btnAceparNotificacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceparNotificacionActionPerformed
        if (validarNotificacion()) {
            AvisoTable aviso;
            if (nuevoRegistro) {
                aviso = new AvisoTable();

            } else {
                aviso = listNotificaciones.getElement(jlistNotificaciones.getSelectedIndex());
            }

            aviso.setTipo(jcbTipoNotificacion.getSelectedItem().toString());
            aviso.setDescripcion(tfNombreNotificacion.getText());
            aviso.setFecDesde(tfFechaInicioNot.getSelectedDate().getTime());
            aviso.setFecHasta(tfFechaFinNot.getSelectedDate().getTime());

            aviso.setDependienteTable(dependiente);

            if (nuevoRegistro) {
                bd.insert(aviso);
                listNotificaciones.addElement(aviso);
                nuevoRegistro = false;
            } else {
                bd.update(aviso);

            }

            cambiarEstadoNotificacion(false);

        }
    }//GEN-LAST:event_btnAceparNotificacionActionPerformed

    private void btnEliminarRecordatorioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarRecordatorioActionPerformed
        if (jTableNotifiaciones.getSelectedRow() != -1) {
            int i = JOptionPane.showConfirmDialog(this, "Seguro que desea borrar este recordatorio?", "Borrar", JOptionPane.YES_NO_OPTION);
            if (i == JOptionPane.YES_OPTION) {
                AvisoPeriodicidadTable recordatorio = tableNotificaciones.getObject(jTableNotifiaciones.getSelectedRow());

                bd.remove(recordatorio);

                tableNotificaciones.removeObject(recordatorio);

            }
        } else {
            JOptionPane.showMessageDialog(this, "Debes selccionar un recordatorio, para borrarlo");
        }

    }//GEN-LAST:event_btnEliminarRecordatorioActionPerformed

    private void btnCancelarRecordatorioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarRecordatorioActionPerformed
        nuevoRegistro = false;
        cambiarEstadoRecordatorio(false);
        avisoTmp = null;

    }//GEN-LAST:event_btnCancelarRecordatorioActionPerformed

    private void btnAceptarRecordatorioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarRecordatorioActionPerformed
        if (validarRecordatorio()) {
            AvisoPeriodicidadTable a;
            if (nuevoRegistro) {
                a = new AvisoPeriodicidadTable();

            } else {
                a = recordatorioTmp;

            }

            a.setDescripcion(tfDescripcionNotificacion.getText());

            Calendar calendar = Calendar.getInstance();

            calendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(tfHoraInicioNot.getText()));
            calendar.set(Calendar.MINUTE, Integer.valueOf(tfMinutoInicioNot.getText()));
            a.setHoraFecha(calendar.getTime());

            if (nuevoRegistro) {
                a.setVisto(false);

                a.setAvisoTable(avisoTmp);
                tableNotificaciones.addRow(a);
                nuevoRegistro = false;
                bd.insert(a);
            } else {

                bd.update(a);
            }
            if (listNotificaciones.getElement(jlistNotificaciones.getSelectedIndex()).getTipo().equals("Llamada")) {
                vista.LlenarTablaLlamadas();
            }

            cambiarEstadoRecordatorio(false);
        }

    }//GEN-LAST:event_btnAceptarRecordatorioActionPerformed

    private void btnEditarRecordatorioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarRecordatorioActionPerformed
        if (jTableNotifiaciones.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(this, "Debes seleccionar un recordatori para editarlo");
        } else {
            recordatorioTmp = tableNotificaciones.getObject(jTableNotifiaciones.getSelectedRow());
            cambiarEstadoRecordatorio(true);
            Calendar c = Calendar.getInstance();
            c.setTime(recordatorioTmp.getHoraFecha());
            tfHoraInicioNot.setText(String.valueOf(c.get(Calendar.HOUR)));
            tfMinutoInicioNot.setText(String.valueOf(c.get(Calendar.MINUTE)));
            tfDescripcionNotificacion.setText(recordatorioTmp.getDescripcion());
        }
    }//GEN-LAST:event_btnEditarRecordatorioActionPerformed

    private void btnEditarNotificacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarNotificacionActionPerformed
        if (jlistNotificaciones.getSelectedIndex() != -1) {
            avisoTmp = listNotificaciones.getElement(jlistNotificaciones.getSelectedIndex());
            cambiarEstadoNotificacion(true);
            tfNombre.setText(avisoTmp.getDescripcion());
            jcbTipoNotificacion.setSelectedItem(avisoTmp.getTipo());
            Calendar c = Calendar.getInstance();
            c.setTime(avisoTmp.getFecDesde());
            tfFechaInicioNot.setSelectedDate(c);

            c.setTime(avisoTmp.getFecHasta());

            tfFechaFinNot.setSelectedDate(c);
            if (c.get(Calendar.YEAR) == 3000) {
                tfFechaFinNot.setEnabled(false);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione una notificación para editarla.");
        }


    }//GEN-LAST:event_btnEditarNotificacionActionPerformed

    private void btnArribaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnArribaActionPerformed
        if (jlistFamiliares.getSelectedIndex() == -1) {
            JOptionPane.showMessageDialog(this, "Selecciona una persona para cambiar su prioridad.");
        } else {
            if (jlistFamiliares.getSelectedIndex() == 0) {
                JOptionPane.showMessageDialog(this, "Esta persona ya tiene la máxima prioridad.");
            } else {
                if (listFamiliares.getSize() > 1) {
                    listFamiliares.subirPrioridad(jlistFamiliares.getSelectedIndex());
                    bd.update(listFamiliares.getElement(jlistFamiliares.getSelectedIndex()));
                    bd.update(listFamiliares.getElement(jlistFamiliares.getSelectedIndex() - 1));
                }

            }

        }

    }//GEN-LAST:event_btnArribaActionPerformed

    private void btnAbajoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAbajoActionPerformed
        if (jlistFamiliares.getSelectedIndex() == -1) {
            JOptionPane.showMessageDialog(this, "Selecciona una persona para cambiar su prioridad.");
        } else {
            if (jlistFamiliares.getSelectedIndex() == listFamiliares.getSize()) {
                JOptionPane.showMessageDialog(this, "Esta persona ya tiene la menor prioridad.");
            } else {
                if (listFamiliares.getSize() > 1) {
                    listFamiliares.bajarPrioridad(jlistFamiliares.getSelectedIndex());
                    bd.update(listFamiliares.getElement(jlistFamiliares.getSelectedIndex()));
                    bd.update(listFamiliares.getElement(jlistFamiliares.getSelectedIndex() + 1));
                }

            }
        }

    }//GEN-LAST:event_btnAbajoActionPerformed

    private void jcbTipoNotificacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbTipoNotificacionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jcbTipoNotificacionActionPerformed

    private void btnEstablecerPasswdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEstablecerPasswdActionPerformed
        if (vistaPassword == null) {
            vistaPassword = new PasswordDependiente();
        }
        vistaPassword.vaciarPasswd();
        vistaPassword.setVisible(true);
    }//GEN-LAST:event_btnEstablecerPasswdActionPerformed

    private void btnEliminarViviendaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarViviendaActionPerformed
        if (jcbViviendas.getSelectedIndex() == -1) {
            JOptionPane.showMessageDialog(this, "No se han encontrado viviendas");
        } else {

            int i = JOptionPane.showConfirmDialog(this, "Estas seguro de que deseas borrar esta vivienda?");
            if (i == JOptionPane.YES_OPTION) {
                ViviendaTable vivienda = (ViviendaTable) jcbViviendas.getSelectedItem();
                if (vivienda == dependiente.getViviendaTable()) {
                    JOptionPane.showMessageDialog(this, "No se puede borrar la vivienda actual del dependiente");
                } else {
                    bd.remove(vivienda);

                    jcbViviendas.removeItem(vivienda);
                    jcbViviendaActual.removeItem(vivienda);
                }

            }

        }
    }//GEN-LAST:event_btnEliminarViviendaActionPerformed

    private void cbFechaFinStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_cbFechaFinStateChanged

    }//GEN-LAST:event_cbFechaFinStateChanged

    private void cbFechaFinActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbFechaFinActionPerformed
        if (cbFechaFin.isSelected()) {

            tfFechaFinNot.setEnabled(false);
        } else {
            Calendar c = Calendar.getInstance();
            tfFechaFinNot.setEnabled(true);
            tfFechaFinNot.setSelectedDate(c);
        }
    }//GEN-LAST:event_cbFechaFinActionPerformed

    private void labelRealoadMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_labelRealoadMouseClicked
        dependiente = (DependienteTable) bd.getUser(dependiente.getPersonaTable().getDni(), DependienteTable.class);

        cargarDatosDependiente();
        jcbViviendas.getModel().setSelectedItem(dependiente.getViviendaTable());

        initTablaLlamadasAll();
        cargarDatosAviso();
        if (mapa == null) {

            initMaps();

        } else {

            try {
                geo = bd.devolverUltimaCoordenada(ID);
                if (!geo.isEmpty()) {
                    mapa.asignarCoordenadaEspecifica((geo.get(geo.size() - 1)).getLatitud(), (geo.get(geo.size() - 1)).getLongitud());
                    mapa.RecargarImagen();
                    labelMapa.setIcon(mapa.getImagen());
                }
            } catch (IOException ex) {

            }
        }
        JOptionPane.showMessageDialog(this, "Se ha actualizado la información correctamente.");


    }//GEN-LAST:event_labelRealoadMouseClicked
    private void modoEdicionDependiente(Boolean b) {
        tabbedPaneGeneral.setEnabled(!b);
        btnAgregarVivienda.setEnabled(!b);
        btnEliminarVivienda.setEnabled(!b);
        btnEditarViviendas.setEnabled(!b);

        btnNuevoRegistroMedico.setEnabled(!b);
        btnEliminarRegistroMed.setEnabled(!b);
        btnEditarHistorial.setEnabled(!b);

        btnAgregarEstado.setEnabled(!b);
        btnEliminarEstado.setEnabled(!b);
        btnEditarEstado.setEnabled(!b);

        btnAgregarFamiliar.setEnabled(!b);
        btnEliminarFamiliar.setEnabled(!b);
        btnEditarFamiliar.setEnabled(!b);

        bntNuevaNotificacion.setEnabled(!b);
        btnBorrarNotificacion.setEnabled(!b);
        btnEditarNotificacion.setEnabled(!b);

        btnAgregarRocordatorio.setEnabled(!b);
        btnEliminarRecordatorio.setEnabled(!b);
        btnEditarRecordatorio.setEnabled(!b);

    }

    public void ponerMapa(Double latitud, Double longitud) {

        final Browser browser = new Browser();
        com.teamdev.jxbrowser.chromium.swing.BrowserView view = new com.teamdev.jxbrowser.chromium.swing.BrowserView(browser);
        browser.loadURL("https://maps.google.com/?q=" + latitud + "," + longitud);

    }

// <editor-fold defaultstate="collapsed" desc="Variables">       
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bntNuevaNotificacion;
    private javax.swing.JButton btnAbajo;
    private javax.swing.JButton btnAceparNotificacion;
    private javax.swing.JButton btnAceptarEstado;
    private javax.swing.JButton btnAceptarFamiliar;
    private javax.swing.JButton btnAceptarGeneral;
    private javax.swing.JButton btnAceptarHistorial;
    private javax.swing.JButton btnAceptarRecordatorio;
    private javax.swing.JButton btnAceptarVivienda;
    private javax.swing.JButton btnActualizarMapa;
    private javax.swing.JButton btnAgregarEstado;
    private javax.swing.JButton btnAgregarFamiliar;
    private javax.swing.JButton btnAgregarRocordatorio;
    private javax.swing.JButton btnAgregarVivienda;
    private javax.swing.JButton btnArriba;
    private javax.swing.JButton btnBorrarNotificacion;
    private javax.swing.JButton btnCancelaVivienda;
    private javax.swing.JButton btnCancelarEstado;
    private javax.swing.JButton btnCancelarFamiliar;
    private javax.swing.JButton btnCancelarGeneral;
    private javax.swing.JButton btnCancelarHisotial;
    private javax.swing.JButton btnCancelarNotificacion;
    private javax.swing.JButton btnCancelarRecordatorio;
    private javax.swing.JButton btnEditarEstado;
    private javax.swing.JButton btnEditarFamiliar;
    private javax.swing.JButton btnEditarGeneral;
    private javax.swing.JButton btnEditarHistorial;
    private javax.swing.JButton btnEditarNotificacion;
    private javax.swing.JButton btnEditarRecordatorio;
    private javax.swing.JButton btnEditarViviendas;
    private javax.swing.JButton btnEliminarEstado;
    private javax.swing.JButton btnEliminarFamiliar;
    private javax.swing.JButton btnEliminarRecordatorio;
    private javax.swing.JButton btnEliminarRegistroMed;
    private javax.swing.JButton btnEliminarVivienda;
    private javax.swing.JButton btnEstablecerPasswd;
    private javax.swing.JButton btnNuevoRegistroMedico;
    private javax.swing.JButton btnSeguimiento;
    private javax.swing.JButton btnZOOMMas;
    private javax.swing.JButton btnZOOMMenos;
    private javax.swing.JCheckBox cbFechaFin;
    private javax.swing.JCheckBox cbTieneLlaves;
    private javax.swing.JLabel jLFechaFin;
    private javax.swing.JLabel jLHoraFin;
    private javax.swing.JLabel jLHoraInicio;
    private javax.swing.JLabel jLHoraInicio1;
    private javax.swing.JLabel jLMinutoFin;
    private javax.swing.JLabel jLMinutoInicio;
    private javax.swing.JLabel jLMinutoInicio1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLdescripcionEstado;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable jTableEstado;
    private javax.swing.JTable jTableLlamadas;
    private javax.swing.JTable jTableNotifiaciones;
    private javax.swing.JComboBox<CentrosaludTable> jcbCentroSalud;
    private javax.swing.JComboBox<ContactoTable> jcbContactos;
    private javax.swing.JComboBox<String> jcbGenero;
    private javax.swing.JComboBox<MedicoTable> jcbMedico;
    private javax.swing.JComboBox<String> jcbTipoNotificacion;
    private javax.swing.JComboBox<String> jcbTipoVia;
    private javax.swing.JComboBox<String> jcbTipoViaFamiliar;
    private javax.swing.JComboBox<String> jcbTipoVivienda;
    private javax.swing.JComboBox<ViviendaTable> jcbViviendaActual;
    private javax.swing.JComboBox<ViviendaTable> jcbViviendas;
    private javax.swing.JLabel jlFechaInicio;
    private javax.swing.JList<String> jlistFamiliares;
    private javax.swing.JList<String> jlistNotificaciones;
    private javax.swing.JTable jtableHistorial;
    private javax.swing.JLabel labelFechaFin;
    private javax.swing.JLabel labelFechaInicio;
    private javax.swing.JLabel labelMapa;
    private javax.swing.JLabel labelReaload;
    private javax.swing.JPanel panelDescripcion;
    private javax.swing.JPanel panelEstado;
    private javax.swing.JPanel panelH;
    private javax.swing.JPanel panelHistorialEdit;
    private javax.swing.JPanel panelPostal;
    private javax.swing.JPanel panelPostalFamilia;
    private javax.swing.JPanel panelProv;
    private javax.swing.JPanel panelProvFamilia;
    private javax.swing.JPanel panelRecordatorio;
    private javax.swing.JTabbedPane tabbedPaneGeneral;
    private javax.swing.JTextField tfApellido1;
    private javax.swing.JTextField tfApellido1Familiar;
    private javax.swing.JTextField tfApellido2;
    private javax.swing.JTextField tfApellido2Familiar;
    private javax.swing.JTextField tfDDni;
    private javax.swing.JTextArea tfDescripcionEstado;
    private javax.swing.JTextArea tfDescripcionNotificacion;
    private javax.swing.JTextField tfDescripciónHistorial;
    private javax.swing.JTextField tfDireccion;
    private javax.swing.JTextField tfDireccionFamiliar;
    private javax.swing.JTextField tfDisponibilidadfamiliar;
    private javax.swing.JTextField tfDniFamiliar;
    private javax.swing.JTextField tfEmail;
    private javax.swing.JTextField tfEmailFamiliar;
    private javax.swing.JTextField tfEscalera;
    private javax.swing.JTextField tfEscaleraFamiliar;
    private datechooser.beans.DateChooserCombo tfFechaAlta;
    private datechooser.beans.DateChooserCombo tfFechaFin;
    private datechooser.beans.DateChooserCombo tfFechaFinNot;
    private datechooser.beans.DateChooserCombo tfFechaInicio;
    private datechooser.beans.DateChooserCombo tfFechaInicioNot;
    private javax.swing.JTextField tfHoraFin;
    private javax.swing.JTextField tfHoraInicio;
    private javax.swing.JTextField tfHoraInicioNot;
    private javax.swing.JTextField tfLetra;
    private javax.swing.JTextField tfLetraFamiliar;
    private javax.swing.JTextField tfMinutoFin;
    private javax.swing.JTextField tfMinutoInicio;
    private javax.swing.JTextField tfMinutoInicioNot;
    private javax.swing.JTextField tfModoAcceso;
    private datechooser.beans.DateChooserCombo tfNacimiento;
    private javax.swing.JTextField tfNombre;
    private javax.swing.JTextField tfNombreFamiliar;
    private javax.swing.JTextField tfNombreNotificacion;
    private javax.swing.JTextField tfNss;
    private javax.swing.JTextField tfNumero;
    private javax.swing.JTextField tfNumeroFamiliar;
    private javax.swing.JTextField tfObreservaciones;
    private javax.swing.JTextField tfPiso;
    private javax.swing.JTextField tfPisoFamiliar;
    private javax.swing.JTextField tfRelación;
    private javax.swing.JTextField tfTelefono;
    private javax.swing.JTextField tfTelefonoFamilia;
    private javax.swing.JTextField tfTipoHistorial;
    // End of variables declaration//GEN-END:variables
// </editor-fold>     
    private void nuevoExpedinete() {
        tabbedPaneGeneral.setEnabled(false);

        btnAgregarVivienda.setEnabled(false);
        btnEliminarVivienda.setEnabled(false);
        btnEditarViviendas.setEnabled(false);
        jcbViviendas.setEnabled(false);

        cambiarEstadoViviendas(true);
        cambiarEstadoGeneral(true);

    }

    private void cambiarEstadoGeneral(boolean b) {
        tfNombre.setEditable(b);
        tfApellido1.setEditable(b);
        tfApellido2.setEditable(b);
        tfDDni.setEditable(b);
        tfTelefono.setEditable(b);
        tfEmail.setEditable(b);
        tfNacimiento.setEnabled(b);
        tfNss.setEditable(b);
        labelReaload.setVisible(!b);

        jcbViviendaActual.setEnabled(b);
        jcbCentroSalud.setEnabled(b);
        jcbMedico.setEnabled(b);
        jcbGenero.setEnabled(b);
        tfFechaAlta.setEnabled(b);
        btnEstablecerPasswd.setVisible(b);

        btnAceptarGeneral.setVisible(b);
        btnCancelarGeneral.setVisible(b);
        btnEditarGeneral.setVisible(!b);
        modoEdicionDependiente(b);
    }

    private void cargarDatosDependiente() {
        tfNombre.setText(dependiente.getPersonaTable().getNombre());
        tfApellido1.setText(dependiente.getPersonaTable().getApellido1());
        tfApellido2.setText(dependiente.getPersonaTable().getApellido2());
        tfDDni.setText(dependiente.getPersonaTable().getDni());
        tfEmail.setText(dependiente.getPersonaTable().getEmail());
        tfNss.setText(dependiente.getNss());
        tfTelefono.setText(String.valueOf(dependiente.getPersonaTable().getTelefono()));
        tfNacimiento.setEnabled(true);
        tfNacimiento.setSelectedDate(getDateCalendar(dependiente.getFecNacimiento()));
        tfNacimiento.setEnabled(false);
        tfFechaAlta.setEnabled(true);
        tfFechaAlta.setSelectedDate(getDateCalendar(dependiente.getFecAlta()));
        tfFechaAlta.setEnabled(false);

        jcbGenero.setSelectedItem(dependiente.getGenero());

        jcbCentroSalud.getModel().setSelectedItem(dependiente.getCentrosaludTable());
        jcbMedico.getModel().setSelectedItem(dependiente.getMedicoTable());
        jcbViviendas.getModel().setSelectedItem(dependiente.getViviendaTable());
        jcbViviendaActual.getModel().setSelectedItem(dependiente.getViviendaTable());

    }

    private void cambiarEstadoViviendas(Boolean b) {
        btnEditarGeneral.setEnabled(!b);
        tabbedPaneGeneral.setEnabled(!b);
        jcbTipoVia.setEnabled(b);
        tfDireccion.setEditable(b);
        tfNumero.setEditable(b);
        tfPiso.setEditable(b);
        tfLetra.setEditable(b);
        tfEscalera.setEditable(b);
        jcbProvincia.setEnabled(b);
        if (jcbCiudad != null) {
            jcbCiudad.setEnabled(false);
        }

        jcbTipoVivienda.setEnabled(b);
        tfModoAcceso.setEditable(b);
        btnAceptarVivienda.setVisible(b);
        btnCancelaVivienda.setVisible(b);
        btnEditarViviendas.setVisible(!b);

        jcbViviendas.setEnabled(!b);
        btnAgregarVivienda.setVisible(!b);
        btnEliminarVivienda.setVisible(!b);

    }

    private void disableButtonEdit() {
        jcbMedico.getEditor().getEditorComponent().setEnabled(false);
        btnAceptarGeneral.setVisible(false);
        btnAceptarFamiliar.setVisible(false);
        btnAceptarVivienda.setVisible(false);
        btnAceparNotificacion.setVisible(false);
        btnAceptarHistorial.setVisible(false);
        btnAceptarEstado.setVisible(false);

        btnCancelarGeneral.setVisible(false);
        btnCancelarFamiliar.setVisible(false);
        btnCancelaVivienda.setVisible(false);
        btnCancelarNotificacion.setVisible(false);
        btnCancelarHisotial.setVisible(false);
        btnCancelarEstado.setVisible(false);
        btnAceptarRecordatorio.setVisible(false);
        btnCancelarRecordatorio.setVisible(false);
        cambiarEstadoEstado(false);
        btnEstablecerPasswd.setVisible(false);

    }

    public Calendar getDateCalendar(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    private boolean validarPersona() {
        boolean valido = true;
        String error = "Se han encontrado los siguientes errores:\n";
        if (tfDDni.getText().equals("")) {
            valido = false;
            error += "El campo DNI es obligatorio.\n";
        }
        if (tfApellido1.getText().equals("") || tfApellido2.getText().equals("")) {
            valido = false;
            error += "Los campos Apellido son obligatorio.\n";
        }
        if (tfNombre.getText().equals("")) {
            valido = false;
            error += "El campo Nombre es obligatorio.\n";
        }
        if (jcbGenero.getSelectedItem() == null) {
            valido = false;
            error += "Debes seleccionar una opción para el campo Genero\n";

        }

        if (tfTelefono.getText().equals("")) {
            valido = false;
            error += "El campo Telefono es obligatorio.\n";
        } else {
            try {
                Integer.parseInt(tfTelefono.getText());
            } catch (NumberFormatException e) {
                valido = false;
                error += "El campo Telefono debe contener solo numeros.\n";
            }
        }

        if (tfNacimiento.getText().equals("")) {
            valido = false;
            error += "El campo Fecha Nacimiento es obligatorio.\n";
        }
        if (tfEmail.getText().equals("")) {
            valido = false;
            error += "El campo Email es obligatorio.\n";
        }
        if (jcbViviendaActual.getItemCount() == 0) {

            valido = false;
            error += "Debes introducir la información de la vivienda actual, en la sección Viviendas\n";
        } else {
            if (jcbViviendaActual.getSelectedItem() == null) {
                valido = false;
                error += "Debes seleccionar una opción para el campo Vivienda actual\n";
            }
        }

        if (jcbMedico.getSelectedItem() == null) {
            valido = false;
            error += "Debes seleccionar una opción para el campo Medico.\n";
        }
        if (tfFechaAlta.getText().equals("")) {
            valido = false;
            error += "El campo Fecha de alta es obligatorio\n";
        }
        if (tfNss.getText().equals("")) {
            valido = false;
            error += "El campo Número de la seguridad social es obligatorio\n";
        } else {
            try {
                Integer.parseInt(tfNss.getText());
            } catch (NumberFormatException x) {
                valido = false;
                error += "El campo Número de la seguridad social debe ser un numero\n";
            }

        }
        if (jcbCentroSalud.getSelectedItem() == null) {
            valido = false;
            error += "Debes seleccionar una opción para el campo Centro de salud\n";
        }
        if (dependiente == null) {
            if (vistaPassword == null || vistaPassword.getPasswd().equals("")) {
                valido = false;
                error += "Debe establecer una contraseña para el dependiente";
            }

        }

        if (!valido) {
            JOptionPane.showMessageDialog(this, error);
        }

        return valido;
    }

    private boolean validarVivienda() {
        boolean valido = true;
        String error = "Se han encontrado los siguientes errores:\n";
        if (jcbTipoVia.getSelectedItem() == null) {
            valido = false;
            error += "Debes seleccionar una opción para el campo Centro de salud\n";
        }
        if (tfDireccion.getText().equals("")) {
            valido = false;
            error += "El campo Dirección es obligatorio\n";

        }
        if (tfNumero.getText().equals("")) {
            valido = false;
            error += "El campo Número es obligatorio";
        }

        if (jcbTipoVivienda.getSelectedItem() == null) {
            valido = false;
            error += "Debes seleccionar una opción para el campo Tipo de vivienda\n";
        }
        if (jcbProvincia.getSelectedItem() == null) {
            valido = false;
            error += "Deber seleccionar una opción para el campo Provincia";
        }
        if (jcbCiudad.getSelectedItem() == null) {
            valido = false;
            error += "Deber seleccionar una opción para el campo Ciudad";
        }
        if (tfModoAcceso.getText().equals("")) {
            valido = false;
            error += "El campo mo de acceso es obligatorio";
        }

        if (valido == false) {
            JOptionPane.showMessageDialog(this, error);
        }

        return valido;
    }

    private void cargarDatosVivienda() {
        ViviendaTable v = (ViviendaTable) jcbViviendas.getSelectedItem();
        jcbTipoVia.setSelectedItem(v.getDireccionTable().getTipoVia());
        tfDireccion.setText(v.getDireccionTable().getDireccion());
        tfNumero.setText(String.valueOf(v.getDireccionTable().getNum()));

        if (v.getDireccionTable().getPiso() != null) {
            tfPiso.setText(String.valueOf(v.getDireccionTable().getNum()));
        }
        if (v.getDireccionTable().getLetra() != null) {
            tfLetra.setText(v.getDireccionTable().getLetra());
        }
        if (v.getDireccionTable().getEscalera() != null) {
            tfEscalera.setText(v.getDireccionTable().getEscalera());
        }

        jcbProvincia.getModel().setSelectedItem(v.getDireccionTable().getCiudadTable().getProvinciaTable());
        jcbCiudad.getModel().setSelectedItem(v.getDireccionTable().getCiudadTable());
        jcbCiudad.setEnabled(false);
        jcbTipoVivienda.setSelectedItem(v.getTipo());
        tfModoAcceso.setText(v.getModoAcceso());

    }

    private void cambiarEstadoFamiliar(boolean b) {
        tabbedPaneGeneral.setEnabled(!b);
        tfDniFamiliar.setEditable(b);
        tfApellido1Familiar.setEditable(b);
        tfApellido2Familiar.setEditable(b);
        tfNombreFamiliar.setEditable(b);
        tfTelefonoFamilia.setEditable(b);
        tfEmailFamiliar.setEditable(b);
        cbTieneLlaves.setEnabled(b);
        tfDisponibilidadfamiliar.setEditable(b);
        tfObreservaciones.setEditable(b);
        jcbTipoViaFamiliar.setEnabled(b);
        tfDireccionFamiliar.setEditable(b);
        tfPisoFamiliar.setEditable(b);
        tfRelación.setEditable(b);
        tfNumeroFamiliar.setEditable(b);
        tfLetraFamiliar.setEditable(b);
        tfEscaleraFamiliar.setEditable(b);
        jcbProvinciaFamiliar.setEnabled(b);

        if (jcbCiudadFamiliar != null) {
            jcbCiudadFamiliar.setEnabled(false);
        }

        btnEditarGeneral.setEnabled(!b);
        btnEditarFamiliar.setEnabled(!b);
        btnAgregarFamiliar.setEnabled(!b);
        btnEliminarFamiliar.setEnabled(!b);
        btnAceptarFamiliar.setVisible(b);
        btnCancelarFamiliar.setVisible(b);
        jcbContactos.setEnabled(!b);

    }

    private void cambiarEstadoHistorial(Boolean b) {
        tabbedPaneGeneral.setEnabled(!b);
        btnEditarGeneral.setEnabled(!b);
        btnNuevoRegistroMedico.setEnabled(!b);
        btnEliminarRegistroMed.setEnabled(!b);
        btnAceptarHistorial.setVisible(b);
        btnCancelarHisotial.setVisible(b);
        btnEditarHistorial.setEnabled(!b);
        panelHistorialEdit.setVisible(b);

    }

    private void tfDniFamiliarActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
    }

    private void tfApellido1FamiliarActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
    }

    private void tfObreservacionesActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
    }

    private void tfEmailFamiliarActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
    }

    private void tfTelefonoFamiliaActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
    }

    private void tfEscaleraFamiliarActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
    }

    private void tfDireccionFamiliarActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
    }

    private void btnBorrarNotificacionActionPerformed(java.awt.event.ActionEvent evt) {
        if (jlistNotificaciones.getSelectedIndex() != -1) {
            int i = JOptionPane.showConfirmDialog(this, "Seguro que desea borrar esta notificación?");
            if (i == JOptionPane.YES_OPTION) {
                avisoTmp = listNotificaciones.getElement(jlistNotificaciones.getSelectedIndex());
                bd.remove(avisoTmp);
                if (avisoTmp.getTipo().equals("Llamada")) {
                    vista.LlenarTablaLlamadas();

                }
                listNotificaciones.removeElement(jlistNotificaciones.getSelectedIndex());
                for (int x = 0; x < tableNotificaciones.getDatos().size(); x++) {
                    AvisoPeriodicidadTable avs = tableNotificaciones.getObject(x);
                    if (avs.getAvisoTable().equals(avisoTmp)) {
                        bd.remove(avs);
                    }
                }
            }
        } else {
            JOptionPane.showMessageDialog(this, "Debes seleccionar una notificación para borrarla.");
        }

    }

    private void bntNuevaNotificacionActionPerformed(java.awt.event.ActionEvent evt) {
        cambiarEstadoNotificacion(true);
        nuevoRegistro = true;
        tfNombreNotificacion.setText("");
        tableNotificaciones.removeAll();
        jcbTipoNotificacion.setSelectedIndex(-1);
        Calendar c = Calendar.getInstance();
        tfFechaInicioNot.setSelectedDate(c);
        tfFechaFinNot.setSelectedDate(c);

    }

    private void btnSeguimientoActionPerformed(java.awt.event.ActionEvent evt) {

        try {

            if (!geo.isEmpty()) {

                SeguimientoGeolocalizacion seguimiento = new SeguimientoGeolocalizacion(geo.get(geo.size() - 1).getLatitud(), geo.get(geo.size() - 1).getLongitud());
                seguimiento.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
                seguimiento.setLocationRelativeTo(null);
                seguimiento.setVisible(true);

            } else {

            }
        } catch (ParseException ex) {
            Logger.getLogger(Expediente.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void btnActualizarMapaActionPerformed(java.awt.event.ActionEvent evt) {

        try {
            if (mapa == null) {

                initMaps();

            } else {

                geo = bd.devolverUltimaCoordenada(ID);
                mapa.asignarCoordenadaEspecifica((geo.get(geo.size() - 1)).getLatitud(), (geo.get(geo.size() - 1)).getLongitud());
                mapa.RecargarImagen();
                labelMapa.setIcon(mapa.getImagen());
            }
        } catch (IOException ex) {

        } catch (ArrayIndexOutOfBoundsException e) {

            labelMapa.setIcon(null);
            labelMapa.setText("Este dependiente no tiene una Localizacion asignada");
            labelMapa.setVerticalAlignment(SwingConstants.CENTER);
            labelMapa.setHorizontalAlignment(SwingConstants.CENTER);

        }

    }

    private void btnAceptarFamiliarActionPerformed(java.awt.event.ActionEvent evt) {
        if (validarFamiliar()) {
            FamiliarTable familiar;
            PersonaTable persona;
            DireccionTable direccion;
            ContactoTable contacto;

            if (nuevoRegistro) {
                familiar = new FamiliarTable();
                persona = new PersonaTable();
                direccion = new DireccionTable();
                contacto = new ContactoTable();
                familiar.setPersonaTable(persona);
                familiar.setDireccionTable(direccion);
                contacto.setDependienteTable(dependiente);
                contacto.setFamiliarTable(familiar);
                contacto.setPrioridad(listFamiliares.getSize());
                familiar.setRelacion(tfRelación.getText());

            } else {

                contacto = (ContactoTable) jcbContactos.getSelectedItem();
                familiar = contacto.getFamiliarTable();
                persona = familiar.getPersonaTable();
                direccion = familiar.getDireccionTable();
            }
            persona.setDni(tfDniFamiliar.getText());
            persona.setApellido1(tfApellido1Familiar.getText());
            persona.setApellido2(tfApellido2Familiar.getText());
            persona.setNombre(tfNombreFamiliar.getText());
            persona.setTelefono(tfTelefonoFamilia.getText());
            persona.setEmail(tfEmailFamiliar.getText());

            familiar.setLlaves(cbTieneLlaves.isSelected());
            familiar.setDisponibilidad(tfDisponibilidadfamiliar.getText());
            familiar.setObservaciones(tfObreservaciones.getText());

            direccion.setTipoVia(jcbTipoViaFamiliar.getSelectedItem().toString());
            direccion.setDireccion(tfDireccionFamiliar.getText());
            direccion.setNum(Integer.valueOf(tfNumeroFamiliar.getText()));
            if (!tfPisoFamiliar.getText().equals("")) {
                direccion.setPiso(Integer.valueOf(tfPisoFamiliar.getText()));
            }
            if (!tfLetraFamiliar.getText().equals("")) {
                direccion.setLetra(tfLetraFamiliar.getText());
            }
            if (!tfEscaleraFamiliar.getText().equals("")) {
                direccion.setEscalera(tfEscaleraFamiliar.getText());
            }
            direccion.setCiudadTable((CiudadTable) jcbCiudadFamiliar.getSelectedItem());
            //SET PRIORIDAD

            if (nuevoRegistro) {

                bd.insert(direccion);
                bd.insert(persona);
                bd.insert(familiar);
                bd.insert(contacto);
                nuevoRegistro = false;
                listFamiliares.addElement(contacto);
                jcbContactos.addItem(contacto);
                jcbContactos.setSelectedItem(contacto);

            } else {
                bd.update(direccion);
                bd.update(persona);
                bd.update(familiar);
                bd.update(contacto);

            }

            cambiarEstadoFamiliar(false);
        }
    }

    private boolean validarRegistro() {
        boolean valido = true;
        String error = "Se han encontrado los siguientes errores:\n";
        if (tfTipoHistorial.getText().equals("")) {
            valido = false;
            error += "El campo tipo no puede estar vacio\n";
        }
        if (tfDescripciónHistorial.getText().equals("")) {
            valido = false;
            error += "El campo descripcion no puede estar vacio\n";
        }

        if (!valido) {
            JOptionPane.showMessageDialog(this, error);
        }
        return valido;
    }

    private boolean validarFamiliar() {
        boolean valido = true;
        String error = "Se han encotnrado los siguientes errores:\n";
        if (tfDniFamiliar.getText().equals("")) {
            valido = false;
            error += "El campo Dni es obligatorio\n";
        }
        if (tfApellido1Familiar.getText().equals("")) {
            valido = false;
            error += "El campo 1 Apellido es obligatorio\n";
        }
        if (tfApellido2Familiar.getText().equals("")) {
            valido = false;
            error += "El campo 2 Apellido es obligatorio\n";
        }
        if (tfTelefonoFamilia.getText().equals("")) {
            valido = false;
            error += "El campo telefono es obligatorio\n";
        }
        try {
            Integer.valueOf(tfTelefonoFamilia.getText());
        } catch (NumberFormatException e) {
            valido = false;
            error += "El campo telefono debe ser un número\n";
        }
        if (jcbTipoViaFamiliar.getSelectedIndex() == -1) {
            valido = false;
            error += "Debe seleccionar un tipo de vian\n";
        }
        if (tfDireccionFamiliar.getText().equals("")) {
            valido = false;
            error += "El campo Dirección es obligatorio\n";
        }
        if (tfApellido1Familiar.getText().equals("")) {
            valido = false;
            error += "El campo Num es obligatorio\n";
        }

        if (!valido) {
            JOptionPane.showMessageDialog(this, error);
        }

        return valido;
    }

    private void cargarDatosContactos() {
        if (jcbContactos.getSelectedItem() != null) {
            ContactoTable contacto = (ContactoTable) jcbContactos.getSelectedItem();
            tfDniFamiliar.setText(contacto.getFamiliarTable().getPersonaTable().getDni());
            tfApellido1Familiar.setText(contacto.getFamiliarTable().getPersonaTable().getApellido1());
            tfApellido2Familiar.setText(contacto.getFamiliarTable().getPersonaTable().getApellido2());
            tfNombreFamiliar.setText(contacto.getFamiliarTable().getPersonaTable().getNombre());
            tfTelefonoFamilia.setText(contacto.getFamiliarTable().getPersonaTable().getTelefono());
            tfEmailFamiliar.setText(contacto.getFamiliarTable().getPersonaTable().getEmail());
            tfRelación.setText(contacto.getFamiliarTable().getRelacion());
            if (contacto.getFamiliarTable().getLlaves()) {
                cbTieneLlaves.setSelected(true);
            } else {
                cbTieneLlaves.setSelected(false);
            }

            tfDisponibilidadfamiliar.setText(contacto.getFamiliarTable().getDisponibilidad());
            tfObreservaciones.setText(contacto.getFamiliarTable().getObservaciones());
            DireccionTable dir = contacto.getFamiliarTable().getDireccionTable();

            jcbTipoViaFamiliar.setSelectedItem(dir.getTipoVia());
            tfDireccionFamiliar.setText(dir.getDireccion());
            tfNumeroFamiliar.setText(String.valueOf(dir.getNum()));
            if (dir.getPiso() != null) {
                tfPisoFamiliar.setText(String.valueOf(dir.getPiso()));
            }
            if (dir.getLetra() != null) {
                tfLetraFamiliar.setText(dir.getLetra());
            }
            if (dir.getEscalera() != null) {
                tfEscaleraFamiliar.setText(dir.getEscalera());
            }

            jcbProvinciaFamiliar.getModel().setSelectedItem(dir.getCiudadTable().getProvinciaTable());
            jcbCiudadFamiliar.getModel().setSelectedItem(dir.getCiudadTable());
            jcbCiudadFamiliar.setEnabled(false);
        }

    }

    private void cambiarEstadoEstado(boolean b) {
        tabbedPaneGeneral.setEnabled(!b);
        btnAgregarEstado.setEnabled(!b);
        btnEliminarEstado.setEnabled(!b);
        btnEditarEstado.setEnabled(!b);
        btnEditarGeneral.setEnabled(!b);

        btnAceptarEstado.setVisible(b);
        btnCancelarEstado.setVisible(b);

        jlFechaInicio.setVisible(b);
        tfFechaInicio.setVisible(b);

        jLHoraInicio.setVisible(b);
        tfHoraInicio.setVisible(b);

        jLMinutoInicio.setVisible(b);
        tfMinutoInicio.setVisible(b);

        jLdescripcionEstado.setVisible(b);
        panelDescripcion.setVisible(b);

        jLFechaFin.setVisible(b);
        tfFechaFin.setVisible(b);

        jLHoraFin.setVisible(b);
        tfHoraFin.setVisible(b);

        jLMinutoFin.setVisible(b);
        tfMinutoFin.setVisible(b);

    }

    private boolean validarEstado() {
        boolean valido = true;
        String error = "Se han encontrado los siguientes errores:\n";
        int hora;
        if (tfHoraInicio.getText().equals("")) {
            valido = false;
            error += "El campo hora de inicio es obligatorio";

        } else {
            try {
                hora = Integer.valueOf(tfHoraInicio.getText());
                if (hora > 23) {
                    valido = false;
                    error += "El campo hora de inicio no puede ser superior a 23\n";
                }
            } catch (NumberFormatException ex) {
                valido = false;
                error += "El campo hora de inicio debe ser un número\n";
            }

        }
        if (tfHoraFin.getText().equals("")) {
            valido = false;
            error += "El campo hora fin es obligatorio";
        } else {
            try {
                hora = Integer.valueOf(tfHoraFin.getText());
                if (hora > 23) {
                    valido = false;
                    error += "El campo hora fin no puede ser superior a 23\n";
                }
            } catch (NumberFormatException ex) {
                valido = false;
                error += "El campo hora fin debe ser un número\n";
            }
        }
        if (tfMinutoInicio.getText().equals("")) {
            tfMinutoInicio.setText("00");

        } else {
            try {
                hora = Integer.valueOf(tfMinutoInicio.getText());
                if (hora > 60) {
                    valido = false;
                    error += "El campo minuto de inicio no puede ser superior a 60\n";
                }
            } catch (NumberFormatException ex) {
                valido = false;
                error += "El campo minuto de inicio debe ser un número\n";
            }
        }
        if (tfMinutoFin.getText().equals("")) {
            tfMinutoFin.setText("00");

        } else {
            try {
                hora = Integer.valueOf(tfMinutoFin.getText());
                if (hora > 60) {
                    valido = false;
                    error += "El campo minuto fin no puede ser superior a 60\n";
                }
            } catch (NumberFormatException ex) {
                valido = false;
                error += "El campo minuto fin debe ser un número\n";
            }
        }
        if (tfDescripcionEstado.getText().equals("")) {
            valido = false;
            error += "El campo descrpcion es obligatorio";
        }
        if (!valido) {
            JOptionPane.showMessageDialog(this, error);
        }

        return valido;
    }

    private void cambiarEstadoNotificacion(boolean b) {
        tabbedPaneGeneral.setEnabled(!b);
        btnEditarNotificacion.setVisible(!b);
        btnAceparNotificacion.setVisible(b);
        btnCancelarNotificacion.setVisible(b);
        cbFechaFin.setEnabled(b);
        bntNuevaNotificacion.setEnabled(!b);
        btnBorrarNotificacion.setEnabled(!b);
        btnEditarRecordatorio.setEnabled(!b);
        jlistNotificaciones.setEnabled(!b);
        tfFechaInicioNot.setEnabled(b);

        tfFechaFinNot.setEnabled(b);

        tfNombreNotificacion.setEditable(b);
        jcbTipoNotificacion.setEnabled(b);

        btnAgregarRocordatorio.setEnabled(!b);
        btnEliminarRecordatorio.setEnabled(!b);
    }

    private boolean validarNotificacion() {
        boolean valido = true;
        String error = "Se han encontrado los siguientes erroes:\n";
        if (tfNombreNotificacion.getText().equals("")) {
            valido = false;
            error += "El campo Nombre no puede estar vacio\n";

        }

        if (jcbTipoNotificacion.getSelectedIndex() == -1) {
            valido = false;
            error += "Seleccione un valor para el campo Tipo";

        }
        if (tfFechaFinNot.getSelectedDate() == null) {

        }
        if (cbFechaFin.isSelected()) {
            Calendar c = Calendar.getInstance();
            c.set(Calendar.YEAR, 3000);
            tfFechaFinNot.setSelectedDate(c);
        }

        if (!valido) {
            JOptionPane.showMessageDialog(this, error);
        }
        return valido;
    }

    private void cambiarEstadoRecordatorio(boolean b) {
        tabbedPaneGeneral.setEnabled(!b);
        btnEditarNotificacion.setEnabled(!b);
        btnEditarRecordatorio.setEnabled(!b);
        btnAgregarRocordatorio.setEnabled(!b);
        btnEliminarRecordatorio.setEnabled(!b);
        btnAceptarRecordatorio.setVisible(b);
        btnCancelarRecordatorio.setVisible(b);
        btnEditarGeneral.setEnabled(!b);
        bntNuevaNotificacion.setEnabled(!b);
        btnBorrarNotificacion.setEnabled(!b);
        btnEditarNotificacion.setEnabled(!b);
        panelRecordatorio.setVisible(b);
        jlistNotificaciones.setEnabled(!b);
    }

    private boolean validarRecordatorio() {
        boolean valido = true;
        String error = "Se han encontrado los siguientes errores:\n";
        int hora;
        if (tfHoraInicioNot.getText().equals("")) {
            valido = false;
            error += "El campo hora es obligatorio";

        } else {
            try {
                hora = Integer.valueOf(tfHoraInicioNot.getText());
                if (hora > 23) {
                    valido = false;
                    error += "El campo hora no puede ser superior a 23\n";
                }
            } catch (NumberFormatException ex) {
                valido = false;
                error += "El campo hora debe ser un número\n";
            }

        }
        if (tfMinutoInicioNot.getText().equals("")) {
            tfMinutoInicioNot.setText("00");

        } else {
            try {
                hora = Integer.valueOf(tfMinutoInicioNot.getText());
                if (hora > 60) {
                    valido = false;
                    error += "El campo minuto no puede ser superior a 60\n";
                }
            } catch (NumberFormatException ex) {
                valido = false;
                error += "El campo minuto debe ser un número\n";
            }
        }
        if (tfDescripcionNotificacion.getText().equals("")) {
            valido = false;
            error += "El campo descrpcion es obligatorio";
        }
        if (!valido) {
            JOptionPane.showMessageDialog(this, error);
        }

        return valido;

    }

    private void cargarDatosAviso() {
        if (jlistNotificaciones.getSelectedIndex() != -1 || jlistNotificaciones.getModel().getSize() != 0) {
            
            AvisoTable aviso = listNotificaciones.getElement(jlistNotificaciones.getSelectedIndex());
            tfNombreNotificacion.setText(aviso.getDescripcion());
            jcbTipoNotificacion.setSelectedItem(aviso.getTipo());

            tfFechaInicioNot.setEnabled(true);
            Calendar c = Calendar.getInstance();
            c.setTime(aviso.getFecDesde());
            tfFechaInicioNot.setSelectedDate(c);
            tfFechaInicioNot.setEnabled(false);

            tfFechaFinNot.setEnabled(true);
            c.setTime(aviso.getFecHasta());
            tfFechaFinNot.setSelectedDate(c);
            tfFechaFinNot.setEnabled(false);

            cargarDatosRecordatorio(aviso);

            if (c.get(Calendar.YEAR) == 3000) {
                cbFechaFin.setSelected(true);
            } else {
                cbFechaFin.setSelected(false);
            }
        }

    }

    private void cargarDatosRecordatorio(AvisoTable aviso) {
        tableNotificaciones.removeAll();
        bd.llenarAvisosPeriodicidad(tableNotificaciones, aviso.getId());
    }

    public void initTablaLlamadasAll() {
        modeloLlamdas.removeAll();
        ArrayList<LlamadaTable> todasLlamadasDependiente = bd.DevolverTodasLlamadasHistorialDependiente(dependiente.getId());
        if (!todasLlamadasDependiente.isEmpty()) {
            for (LlamadaTable todasLlamadasDependiente1 : todasLlamadasDependiente) {
                Object[] fila = new Object[4];
                fila[0] = todasLlamadasDependiente1.getId();
                fila[1] = todasLlamadasDependiente1.getFechaHora();
                fila[2] = todasLlamadasDependiente1.getAsistenteTable().getPersonaTable().getNombreCompleto();
                fila[3] = todasLlamadasDependiente1.getRazonLlamada();
                modeloLlamdas.addRow(fila);
            }

        }

    }

    public DependienteTable getDependiente() {
        return dependiente;
    }

}
