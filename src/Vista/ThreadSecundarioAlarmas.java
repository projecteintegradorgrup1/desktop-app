/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.ControladorBD;
import Modelo.DependienteTable;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JList;

/**
 *
 * @author Asif
 */
public class ThreadSecundarioAlarmas extends Thread {

    Socket sc;
    private VistaPrincipal vistaPrincipal;
    public boolean global = false;

    public ThreadSecundarioAlarmas(Socket sc, VistaPrincipal vistaPrincipal) {
        this.sc = sc;
        this.vistaPrincipal = vistaPrincipal;
    }

    @Override
    public void run() {
        BufferedReader bf = null;
        try {
            bf = new BufferedReader(new InputStreamReader(sc.getInputStream()));
            String dni = bf.readLine();
            DependienteTable dependiente = (DependienteTable) new ControladorBD().getUser(dni, DependienteTable.class);
            JList listaAlarmas = vistaPrincipal.getJlistAlarmas();
            ((DefaultListModel<DependienteTable>) listaAlarmas.getModel()).addElement(dependiente);
            vistaPrincipal.encenderTrianguloAlarma(true);

            
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(sc.getOutputStream()));
            bw.write("OK");
            bw.newLine();
            bw.flush();

        } catch (IOException ex) {
            Logger.getLogger(ThreadSecundarioAlarmas.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                bf.close();
            } catch (IOException ex) {
                Logger.getLogger(ThreadSecundarioAlarmas.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
