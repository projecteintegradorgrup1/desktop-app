/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Asif
 */
public class AlarmaThread extends Thread {

    private ServerSocket servidor = null;
    private final int PUERTO = 2020;
    private final VistaPrincipal vistaPrincipal;
    public ThreadSecundarioAlarmas threadServidor;
    private boolean run;

    public AlarmaThread(VistaPrincipal vistaPrincipal) {
        this.vistaPrincipal = vistaPrincipal;
        run = true;
    }

    @Override
    public void run() {

        try {

            servidor = new ServerSocket(PUERTO);
            while (run) {

                Socket sc = servidor.accept();

                threadServidor = new ThreadSecundarioAlarmas(sc, vistaPrincipal);
                threadServidor.start();

            }

        } catch (SocketException ex) {

        } catch (IOException ex) {

            Logger.getLogger(AlarmaThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void stopService() {
        run = false;
        try {
            if (servidor != null) {
                servidor.close();
            }
        } catch (IOException ex) {

        }
    }

}
