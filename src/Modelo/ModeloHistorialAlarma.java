/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;

public class ModeloHistorialAlarma extends AbstractTableModel{
    
    ArrayList datos = new ArrayList();
    String[] columnas = {"ID", "Dependiente", "Fecha y Hora", "Asistente", "Motivo de Alarma"};
    Class[] types = new Class[]{
        java.lang.Integer.class, java.lang.String.class, java.lang.String.class , java.lang.String.class,
        java.lang.String.class};

    
    public ModeloHistorialAlarma() {
    }


    public String getColumnName(int col) {
        return columnas[col];
    }

    

    public int getRowCount() {
        return datos.size();
    }



     public int getColumnCount() {
        return columnas.length;
    }



    public Object getValueAt(int row, int col) {
        Object[] fila = (Object[]) datos.get(row);
        return fila[col];
    }

    public Class getColumnClass(int columnIndex) {
        return types[columnIndex];
    }

    public void addRow(Object[] fila) {
        datos.add(fila);
        fireTableDataChanged();
    }

    @Override
    public void setValueAt(Object value, int row, int col) {
        Object[] fila = (Object[]) datos.get(row);
        fila[col] = value;
        fireTableCellUpdated(row, col);
    }
    
}
