/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author vesprada
 */
public class TableModelEstado extends AbstractTableModel {

    private ArrayList<EstadoTable> datos;
    private final String[] columnas;
    private int rowEdit;

    public TableModelEstado() {
        datos = new ArrayList();
        columnas = new String[]{"Fecha Inicio", "Fecha Fin", "Descripción"};
        rowEdit = -1;
    }

    @Override
    public int getRowCount() {
        return datos.size();
    }

    @Override
    public int getColumnCount() {
        return columnas.length;
    }

    public void addRow(EstadoTable estado) {
        datos.add(estado);
        fireTableDataChanged();
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        boolean edit;
        switch (rowEdit) {
            case -1:
                edit = false;
                break;
            case -2:
                edit = true;
                break;
            default:
                edit = rowIndex == rowEdit;
                break;
        }
        return edit; //To change body of generated methods, choose Tools | Templates.
    }

    public void setRowEditable(int row) {
        rowEdit = row;
        isCellEditable(rowEdit, 0);
    }

    @Override
    public String getColumnName(int column) {
        return columnas[column];
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        super.setValueAt(aValue, rowIndex, columnIndex);
        EstadoTable estado = datos.get(rowIndex);

        switch (columnIndex) {
            case 0:

                estado.setFechaHoraInicio((Date) aValue);

                break;
            case 1:
                estado.setFechaHora((Date) aValue);
                break;
            case 2:
                estado.setDescripcion((String) aValue);
                break;
            default:
                break;
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object value = null;
        EstadoTable estadoTable = datos.get(rowIndex);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY HH:mm");
        switch (columnIndex) {
            case 0:
                value = sdf.format(estadoTable.getFechaHoraInicio());
                break;
            case 1:
                value = sdf.format(estadoTable.getFechaHora());
                break;
            case 2:
                value = estadoTable.getDescripcion();
                break;
            default:
                break;
        }
        return value;
    }

    public EstadoTable getObject(int row) {
        return datos.get(row);
    }

    public void removeObject(EstadoTable estadoTable) {
        datos.remove(estadoTable);
        fireTableDataChanged();
    }

}
