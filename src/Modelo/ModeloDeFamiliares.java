/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;


public class ModeloDeFamiliares extends AbstractTableModel{
  
    ArrayList datos = new ArrayList();
    String[] columnas = {"Prioridad", "Nombre Familiar", "Disponibilidad"};
    Class[] types = new Class[]{
        java.lang.Integer.class, java.lang.String.class,java.lang.String.class};

    public ModeloDeFamiliares() {
    
        datos = new ArrayList();
        
    }
    
    
    
    public String getColumnName(int col) {
        return columnas[col];
    }

    
    @Override
    public int getRowCount() {
        return datos.size();
    }


    @Override
     public int getColumnCount() {
        return columnas.length;
    }


    @Override
    public Object getValueAt(int row, int col) {
        Object[] fila = (Object[]) datos.get(row);
        return fila[col];
    }

    public Class getColumnClass(int columnIndex) {
        return types[columnIndex];
    }

    public void addRow(Object[] fila) {
        datos.add(fila);
        fireTableDataChanged();
    }

    @Override
    public void setValueAt(Object value, int row, int col) {
        Object[] fila = (Object[]) datos.get(row);
        fila[col] = value;
        fireTableCellUpdated(row, col);
    }

    
}
