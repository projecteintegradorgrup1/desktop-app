/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.File;

/**
 *
 * @author vesprada
 */
public class VariablesEstaticas {

    public static class consultasBD {

        public static final String GET_ASISTENTE = "from AsistenteTable a join fetch a.personaTable p where a.personaTable.email = ?";
        public static final String GET_MEDICO = "from MedicoTable a";
        public static final String GET_CENTROSALUD = "from CentrosaludTable c";
        public static final String GET_VIVIENDAS = "from ViviendaTable v left join fetch v.direccionTable d left join fetch d.ciudadTable c left join fetch c.provinciaTable where v.dependienteTable.id=?";
        public static final String GET_GEOLOCALIZACION = "from GeolocalizacionTable a where a.dependienteTable.id = ?";
        public static final String GET_OFRECER_RC = "from OfrecerRcTable o where o.ciudadTable.id = (select c.id from DependienteTable d join d.viviendaTable.direccionTable.ciudadTable c where d.id = ?)";
        public static final String GET_ID_RECURSO = "from RecursoComTable r where r.nombre = ?";
        public static final String GET_PROVINCIAS = "from ProvinciaTable p";
        public static final String GET_ALL_CIUDADES = "from CiudadTable c left join fetch c.provinciaTable";
        public static final String GET_CIUDADES_PROVINCIA = "from CiudadTable c left join fetch c.provinciaTable where c.provinciaTable.id = ?";
        public static final String GET_DEPENDIENTE = "from DependienteTable d left join fetch d.personaTable "
                + "left join fetch d.viviendaTable v left join fetch v.direccionTable dir left join fetch dir.ciudadTable ci left join fetch ci.provinciaTable "
                + "left join fetch d.centrosaludTable "
                + "left join fetch d.medicoTable "
                + "where d.personaTable.dni=?";
        public static final String GET_ALL_DEPENDIENTES = "from DependienteTable d left join fetch d.personaTable "
                + "left join fetch d.viviendaTable v left join fetch v.direccionTable dir left join fetch dir.ciudadTable ci left join fetch ci.provinciaTable "
                + "left join fetch d.centrosaludTable "
                + "left join fetch d.medicoTable";
        //:today between a.fecDesde and a.fecHasta' and 
        public static final String GET_LLAMADAS_A_REALIZAR = "from AvisoTable a left join fetch a.dependienteTable d left join fetch d.personaTable WHERE a.tipo = 'Llamada' ";
        public static final String GET_LLAMADAS_A_REALIZAR2 = "from AvisoPeriodicidadTable p where p.avisoTable.id = ? ";
        public static final String GET_HISTORIAL_MEDICO = "from HistMedicoTable h where h.dependienteTable.id= ? ";
        public static final String GET_LLAMADAS_DEPENDIENTE = "from LlamadaTable l left join fetch l.asistenteTable a left join fetch l.avisoTable v left join fetch a.personaTable left join fetch v.dependienteTable d where d.id = ?";
        public static final String GET_TODAS_LLAMADAS_HISTORIAL = "from LlamadaTable l left join fetch l.asistenteTable a left join fetch a.personaTable";
        public static final String GET_TODAS_ALARMAS = "from AlarmaTable m left join fetch m.asistenteTable a left join fetch a.personaTable left join fetch m.dependienteTable d left join fetch d.personaTable";
        public static final String GET_CONTACTOS = "from ContactoTable c "
                + "left join fetch c.familiarTable f "
                + "left join fetch f.personaTable p "
                + "left join fetch f.direccionTable d "
                + "left join fetch d.ciudadTable ci "
                + "left join fetch ci.provinciaTable pro "
                + "where c.dependienteTable.id = ? "
                + "order by c.prioridad";
        public static final String GET_ESTADOS = "from EstadoTable e where e.dependienteTable.id= ?";
        public static final String GET_CONTACTOS_EMERGENCIA = "from ContactoTable c "
                + "left join fetch c.familiarTable f "
                + "left join fetch f.personaTable p "
                + "left join fetch f.direccionTable d "
                + "left join fetch d.ciudadTable ci "
                + "left join fetch ci.provinciaTable pro "
                + "where c.dependienteTable.id = ? order by c.prioridad ASC";
        public static final String GET_AVISO = "from AvisoTable a where a.dependienteTable.id = ?";
        public static final String GET_AVISO_PERIODICIDAD = "from AvisoPeriodicidadTable a left join fetch a.avisoTable avi where a.avisoTable.id = ?";
    }

    public static class imagenes {

        private static final String IMG_DIR = System.getProperty("user.dir") + File.separator + "Img" + File.separator;
        public static final String BOMBEROS = IMG_DIR + "llama.png";
        public static final String POLICIA = IMG_DIR + "Policia.png";
        public static final String MEDICOS = IMG_DIR + "cruzRoja.png";
        public static final String X = IMG_DIR + "x.png";
        public static final String BOTON_DISPONIBLE = IMG_DIR + "icon_Disponible.png";
        public static final String BOTON_OCUPADO = IMG_DIR + "icon_Ocupado.png";
        public static final String ALERTA_GIF = IMG_DIR + "alerta.gif";
        public static final String ALERTA_NULL = IMG_DIR + "alertanull.png";
        public static final String RELOAD = IMG_DIR+"reload.png";

    }
}
