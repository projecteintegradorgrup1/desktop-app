package Modelo;
//
import Controlador.ControladorBD;
import Modelo.CiudadTable;
import Modelo.ProvinciaTable;
import com.teamdev.jxbrowser.chromium.bd;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

public class FilterComboBox extends JComboBox implements ItemListener{
    private List<Object> array;
     private ControladorBD bd;

    public FilterComboBox(List<Object> array) {
        super(array.toArray());
        this.array = array;
        this.setEditable(true);
        final JTextField textfield = (JTextField) this.getEditor().getEditorComponent();
        textfield.addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent ke) {
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        comboFilter(textfield.getText());
                    }
                });
            }
        });

    }
    
    public void comboFilter(String enteredText) {
        List<Object> filterArray= new ArrayList<Object>();
        if(array.get(0) instanceof ProvinciaTable){
            for (int i = 0; i < array.size(); i++) {
                if (((ProvinciaTable)array.get(i)).getNombre().toLowerCase().contains(enteredText.toLowerCase())) {
                    filterArray.add(array.get(i));
                }
            }   
        }else if( array.get(0) instanceof CiudadTable){
            for (int i = 0; i < array.size(); i++) {
                if (((CiudadTable)array.get(i)).getNombre().toLowerCase().contains(enteredText.toLowerCase())) {
                    filterArray.add(array.get(i));
                }
            }
        }
        
        
        if (filterArray.size() > 0) {
            this.setModel(new DefaultComboBoxModel(filterArray.toArray()));
            this.setSelectedItem(enteredText);
            this.showPopup();
            
        }
        else {
            this.hidePopup();
        }
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        
        if (e.getStateChange() == ItemEvent.SELECTED) {
          ProvinciaTable item = (ProvinciaTable) e.getItem();
          
          item.getId();
          
          
       }
        
        
    }
}