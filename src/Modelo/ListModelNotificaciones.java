/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;
import javax.swing.AbstractListModel;

/**
 *
 * @author JuanC
 */
public class ListModelNotificaciones extends AbstractListModel {

    private ArrayList<AvisoTable> datos;

    public ListModelNotificaciones() {
        datos = new ArrayList<>();
    }

    @Override
    public int getSize() {
        return datos.size();
    }

    @Override
    public Object getElementAt(int i) {
        return datos.get(i);
    }

    public void addElement(AvisoTable aviso) {
        if (!datos.contains(aviso)) {
            datos.add(aviso);
            this.fireIntervalAdded(this, getSize()-1, getSize());
        }

    }

    public void removeElement(int i) {
        datos.remove(i);
        this.fireIntervalRemoved(this, getSize(), getSize() + 1);
    }

    public AvisoTable getElement(int i) {
        return datos.get(i);
    }

    public void removeAll() {
        datos = new ArrayList<>();
        this.fireIntervalRemoved(this, 0, getSize());
    }

}
