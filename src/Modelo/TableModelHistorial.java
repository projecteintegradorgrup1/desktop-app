/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author vesprada
 */
public class TableModelHistorial extends AbstractTableModel {

    private ArrayList<HistMedicoTable> datos;
    private String[] columnas;
    private int rowEdit;

    public TableModelHistorial() {
        datos = new ArrayList();
        columnas = new String[]{"Tipo", "Descripción"};
        rowEdit = -1;
    }

    @Override
    public int getRowCount() {
        return datos.size();
    }

    @Override
    public int getColumnCount() {
        return columnas.length;
    }

    public void addRow(HistMedicoTable hist) {
        datos.add(hist);
        fireTableDataChanged();
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        boolean edit;
        switch (rowEdit) {
            case -1:
                edit = false;
                break;
            case -2:
                edit = true;
                break;
            default:
                edit = rowIndex == rowEdit;
                break;
        }
        return edit; //To change body of generated methods, choose Tools | Templates.
    }

    public void setRowEditable(int row) {
        rowEdit = row;
        isCellEditable(rowEdit, 0);
    }

    @Override
    public String getColumnName(int column) {
        return columnas[column];
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        super.setValueAt(aValue, rowIndex, columnIndex);
        HistMedicoTable hist = datos.get(rowIndex);
        switch (columnIndex) {
            case 0:
                hist.setTipo((String) aValue);
                break;
            case 1:
                hist.setDescripcion((String) aValue);
                break;
            default:
                
                break;
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        String value;
        HistMedicoTable hist = datos.get(rowIndex);
        if (columnIndex == 0) {
            value = hist.getTipo();
        } else {
            value = hist.getDescripcion();
        }
        return value;
    }

    public HistMedicoTable getObject(int row) {
        return datos.get(row);
    }

    public void removeObject(HistMedicoTable hist) {
        datos.remove(hist);
        fireTableDataChanged();
    }

}
