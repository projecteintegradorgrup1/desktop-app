/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;
import javax.swing.AbstractListModel;

/**
 *
 * @author JuanC
 */
public class ListModelFamiliares extends AbstractListModel {

    private ArrayList<ContactoTable> datos;

    public ArrayList<ContactoTable> getDatos() {
        return datos;
    }

    public void setDatos(ArrayList<ContactoTable> datos) {
        this.datos = datos;
    }

    public ListModelFamiliares() {
        datos = new ArrayList<>();
    }

    @Override
    public int getSize() {
        return datos.size();
    }

    @Override
    public Object getElementAt(int i) {
        return datos.get(i);
    }

    public void addElement(ContactoTable contacto) {
        if (!datos.contains(contacto)) {
            datos.add(contacto);
            this.fireIntervalAdded(this, getSize(), getSize() + 1);
        }

    }

    public void removeElement(int i) {
        datos.remove(i);
        this.fireIntervalRemoved(i, getSize(), getSize() + 1);
    }

    public void removeElement(ContactoTable contacto) {

        datos.remove(contacto);
        for (int i = 0; i < datos.size(); i++) {
            datos.get(i).setPrioridad(i);
        }
        this.fireIntervalRemoved(contacto, getSize(), getSize() + 1);
    }

    public ContactoTable getElement(int i) {
        return datos.get(i);
    }

    public void subirPrioridad(int num) {
        ContactoTable baja = datos.get(num - 1);
        baja.setPrioridad(num);
        ContactoTable subeNuevo = datos.get(num);
        subeNuevo.setPrioridad(num - 1);
        datos.set(num - 1, subeNuevo);
        datos.set(num, baja);
        this.fireContentsChanged(this, num - 1, num);
    }

    public void bajarPrioridad(int num) {
        ContactoTable bajaNuevo = datos.get(num);
        bajaNuevo.setPrioridad(num + 1);
        ContactoTable sube = datos.get(num + 1);
        sube.setPrioridad(num);
        datos.set(num, sube);
        datos.set(num + 1, bajaNuevo);
        this.fireContentsChanged(this, num, num + 1);
    }

    public void ordenarPrioridad() {

    }

}
