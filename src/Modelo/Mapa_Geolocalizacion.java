
package Modelo;

import java.awt.Image;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

//Cuando se utiliza uno de los metodos de la clase Mapa_Geolocalizacion, seguidamente ha de cargarse la imagen desde donde se llame el método

public class Mapa_Geolocalizacion {

    public Image imagen = null;
    private URL foto;
    public int ID;
    public double latitud;
    public double longitud;
    public int zoom;
    public String coordenada;

    public Mapa_Geolocalizacion(double latitud, double longitud) {

        try {
            
            this.latitud = latitud;
            this.longitud = longitud;

            zoom = 16;
            this.foto = new URL("https://maps.googleapis.com/maps/api/staticmap?&zoom=" + zoom + "&size=900x400&maptype=roadmap&markers=color:blue|label:D|" + latitud + "," + longitud + "&key=AIzaSyBRocVUhkGQXljjpmniXMiQxJEltGX7oHA");
            coordenada = "https://maps.googleapis.com/maps/api/staticmap?&zoom=" + zoom + "&size=900x400&maptype=roadmap&markers=color:blue|label:D|" + latitud + "," + longitud + "&key=AIzaSyBRocVUhkGQXljjpmniXMiQxJEltGX7oHA";
            //Asignacion de url a la imagen

            this.imagen = ImageIO.read(foto);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    
    //Cuando se utiliza uno de los metodos de la clase Mapa_Geolocalizacion, seguidamente ha de cargarse la imagen desde donde se llame el método
    
    public void Resetear() throws MalformedURLException, IOException {

        
        URL zoomMas = new URL("https://maps.googleapis.com/maps/api/staticmap?&zoom=" + zoom + "&size=900x400&maptype=roadmap&markers=color:blue|label:D|0,0 &key=AIzaSyBRocVUhkGQXljjpmniXMiQxJEltGX7oHA");
        System.out.println("Coordenada Reseteada: https://maps.googleapis.com/maps/api/staticmap?&zoom=" + zoom + "&size=900x400&maptype=roadmap&markers=color:blue|label:D|0,0 &key=AIzaSyBRocVUhkGQXljjpmniXMiQxJEltGX7oHA");
        foto = zoomMas;
        
        //imagen = ImageIO.read(foto);
    }
    
    
    public void RecargarImagen() throws MalformedURLException, IOException {

        URL zoomMas = new URL("https://maps.googleapis.com/maps/api/staticmap?&zoom=" + zoom + "&size=900x400&maptype=roadmap&markers=color:blue|label:D|" + latitud + "," + longitud + "&key=AIzaSyBRocVUhkGQXljjpmniXMiQxJEltGX7oHA");
        foto = zoomMas;
        imagen = ImageIO.read(foto);
    }

    public void Acercar() throws MalformedURLException, IOException {

        if (zoom < 20) {

            zoom++;

        }

        String[] cadena = coordenada.split("&");
        cadena[1] = "zoom=" + zoom;
        coordenada = "";
        for (int i = 0; i < cadena.length; i++) {

            if (i != 0) {
                
                coordenada += "&" + cadena[i];
                
            } else {

                coordenada += cadena[i];

            }

        }

        URL zoomMas = new URL(coordenada);
        foto = zoomMas;
        imagen = ImageIO.read(foto);
    }

    public void Alejar() throws MalformedURLException, IOException {

        if (zoom > 0) {

            zoom--;

        }

        String[] cadena = coordenada.split("&");
        cadena[1] = "zoom=" + zoom;
        coordenada = "";
        for (int i = 0; i < cadena.length; i++) {

            if (i != 0) {

                coordenada += "&" + cadena[i];

            } else {

                coordenada += cadena[i];

            }

        }
        
        URL zoomMenos = new URL(coordenada);
        foto = zoomMenos;
        imagen = ImageIO.read(foto);
    }

    public ImageIcon getImagen() {
        return new ImageIcon(imagen);
    }

    public void setImagen(Image imagen) {
        this.imagen = imagen;
    }
    
    public void asignarCoordenadaEspecifica(double latitud2, double longitud2) throws MalformedURLException, IOException{
    
        boolean continuar = true;
        
        String[] cadena = coordenada.split("&");
        //cadena[3] += "&markers=color:green|label:D|" + latitud2 + "," + longitud2;
        coordenada = "";
        this.latitud = latitud2;
        this.longitud = longitud2;
        
        for (int i = 0; i < cadena.length; i++) {

            if( i == 0 )coordenada = "https://maps.googleapis.com/maps/api/staticmap?";
            if(i == 3){
                
                continuar = false;
            
                coordenada += "&"+cadena[i] +"&markers=color:green|label:D|" + latitud2 + "," + longitud2;
                
            }
            
            if(cadena[i].contains("key")) continuar = true;
            
            if(continuar == true && i != 0){
            
                coordenada += "&"+cadena[i];
                
            }
            

        }
        
        System.out.println("Coordenada Especifica"+coordenada);
        URL zoomMenos = new URL(coordenada);
        foto = zoomMenos;
        imagen = ImageIO.read(foto);
        
    }

    public void anyadirCoordenada(double latitud2, double longitud2) throws MalformedURLException, IOException {

       String[] cadena = coordenada.split("&");
        //cadena[4] += "markers=color:red|label:D|" + latitud2 + "," + longitud2;
        coordenada = "";
        for (int i = 0; i < cadena.length; i++) {

            if( i != 0 && i != 4){
                
                coordenada += "&" + cadena[i];
            
            }else if(i == 4){
                
                coordenada += "&"+cadena[i] + "&markers=color:red|label:D|" + latitud2 + "," + longitud2;
            
            }else{
            
                coordenada += "https://maps.googleapis.com/maps/api/staticmap?";
                
            }

        }
        System.out.println("Coordenada Añadida: "+coordenada);

        URL zoomMenos = new URL(coordenada);
        foto = zoomMenos;
        imagen = ImageIO.read(foto);
    }

}