/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author JuanC
 */
public class TableModelNotificaciones extends AbstractTableModel {

    private ArrayList<AvisoPeriodicidadTable> datos;
    private final String[] columnas;
    private final Class[] types;

    public TableModelNotificaciones() {
        datos = new ArrayList();
        columnas = new String[]{"Hora", "Descripción", "Visto"};
        types = new Class[]{
            java.lang.String.class, java.lang.String.class, java.lang.Boolean.class};
    }

    public ArrayList<AvisoPeriodicidadTable> getDatos() {
        return datos;
    }

    public void setDatos(ArrayList<AvisoPeriodicidadTable> datos) {
        this.datos = datos;
    }

    @Override
    public int getRowCount() {
        return datos.size();
    }

    @Override
    public Class getColumnClass(int columnIndex) {
        return types[columnIndex];
    }

    @Override
    public int getColumnCount() {
        return columnas.length;
    }

    public void addRow(AvisoPeriodicidadTable aviso) {
        datos.add(aviso);
        fireTableDataChanged();
    }

    @Override
    public String getColumnName(int column) {
        return columnas[column];
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        super.setValueAt(aValue, rowIndex, columnIndex);
        AvisoPeriodicidadTable aviso = datos.get(rowIndex);

        switch (columnIndex) {
            case 0:

                aviso.setHoraFecha((Date) aValue);

                break;
            case 1:
                aviso.setDescripcion((String) aValue);
                break;

            case 2:
                aviso.setVisto((Boolean) aValue);
                break;
            default:
                break;
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object value = null;
        AvisoPeriodicidadTable aviso = datos.get(rowIndex);
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        switch (columnIndex) {
            case 0:
                value = sdf.format(aviso.getHoraFecha());
                break;
            case 1:
                value = aviso.getDescripcion();
                break;
            case 2:
                value = aviso.getVisto();
                break;
            default:
                break;
        }
        return value;
    }

    public AvisoPeriodicidadTable getObject(int row) {
        return datos.get(row);
    }

    public void removeObject(AvisoPeriodicidadTable estadoTable) {
        datos.remove(estadoTable);
        fireTableDataChanged();
    }

    public void removeAll() {
        datos = new ArrayList<>();
        fireTableDataChanged();
    }

}
